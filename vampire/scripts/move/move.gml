var amount = argument[0];
var axis = argument[1];
if amount == 0 { return true; }
var target = round_ties_down(subpixel[axis]+amount);
subpixel[axis] += amount-target;
var xa = 1-axis;
var ya = axis;
var s = sign(target);
while target != 0 {
	if !place_free(x+xa*s,y+ya*s) {
		return false;
	}
	x += xa*s;
	y += ya*s;
	target -= s;
}
return true;