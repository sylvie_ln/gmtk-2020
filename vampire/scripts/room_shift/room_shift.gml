var rx = global.rx+argument[0];
var ry = global.ry+argument[1];
var rm = asset_get_index("rm"+string(rx)+string(ry));
if room_exists(rm) {
	global.rx += argument[0];
	global.ry += argument[1];
	return rm;	
} else {
	return -1;	
}