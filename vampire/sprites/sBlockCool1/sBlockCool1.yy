{
    "id": "dfe676e1-6c87-46fd-b77b-eb1ca5704b92",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBlockCool1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a92bc0aa-0ac2-4c88-a46e-d1cbda3daa72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfe676e1-6c87-46fd-b77b-eb1ca5704b92",
            "compositeImage": {
                "id": "6c337c6c-5522-4e8f-a470-7c641f24d142",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a92bc0aa-0ac2-4c88-a46e-d1cbda3daa72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d15da7c-e482-45b6-a340-38f8f5d37999",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a92bc0aa-0ac2-4c88-a46e-d1cbda3daa72",
                    "LayerId": "1f2d6faf-dba3-43df-858e-a8dadf315af9"
                }
            ]
        },
        {
            "id": "3d8ae2ed-4d2c-44c3-8479-28cc7fc228ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfe676e1-6c87-46fd-b77b-eb1ca5704b92",
            "compositeImage": {
                "id": "0fcd2e38-fc8e-4576-90ec-c62eebb39a60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d8ae2ed-4d2c-44c3-8479-28cc7fc228ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c79b7a9-545d-4296-b5cf-6c8a149cae0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d8ae2ed-4d2c-44c3-8479-28cc7fc228ed",
                    "LayerId": "1f2d6faf-dba3-43df-858e-a8dadf315af9"
                }
            ]
        },
        {
            "id": "3cb6c04b-ef70-4312-b3e7-b7105e14201b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfe676e1-6c87-46fd-b77b-eb1ca5704b92",
            "compositeImage": {
                "id": "40b035c2-e8ec-41fb-949c-d457e420af65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cb6c04b-ef70-4312-b3e7-b7105e14201b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0913876e-8565-4046-a0bb-57ab98a24103",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cb6c04b-ef70-4312-b3e7-b7105e14201b",
                    "LayerId": "1f2d6faf-dba3-43df-858e-a8dadf315af9"
                }
            ]
        },
        {
            "id": "5d4864a2-ed98-4b63-9f67-5085f04d9bb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfe676e1-6c87-46fd-b77b-eb1ca5704b92",
            "compositeImage": {
                "id": "16410cf3-0fbb-401b-8012-af266d95edac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d4864a2-ed98-4b63-9f67-5085f04d9bb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6fe451c-810f-4517-8382-8fcc5503494d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d4864a2-ed98-4b63-9f67-5085f04d9bb7",
                    "LayerId": "1f2d6faf-dba3-43df-858e-a8dadf315af9"
                }
            ]
        },
        {
            "id": "6882f449-18ba-48ea-b6a8-8d87b8b6e87b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfe676e1-6c87-46fd-b77b-eb1ca5704b92",
            "compositeImage": {
                "id": "aaadf180-dc8b-470e-a5e0-fbc96a9fe5b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6882f449-18ba-48ea-b6a8-8d87b8b6e87b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5967d1bb-77a1-40c6-bdad-30cfb3e43324",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6882f449-18ba-48ea-b6a8-8d87b8b6e87b",
                    "LayerId": "1f2d6faf-dba3-43df-858e-a8dadf315af9"
                }
            ]
        },
        {
            "id": "04910811-a38c-469e-bf85-6d7dabae044e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfe676e1-6c87-46fd-b77b-eb1ca5704b92",
            "compositeImage": {
                "id": "58462952-99f0-4720-9947-835cc1f912ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04910811-a38c-469e-bf85-6d7dabae044e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0346ecd1-a903-4af3-b0f7-a9278abab8f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04910811-a38c-469e-bf85-6d7dabae044e",
                    "LayerId": "1f2d6faf-dba3-43df-858e-a8dadf315af9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1f2d6faf-dba3-43df-858e-a8dadf315af9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dfe676e1-6c87-46fd-b77b-eb1ca5704b92",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}