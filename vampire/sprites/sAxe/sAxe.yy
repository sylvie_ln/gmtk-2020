{
    "id": "ca51dfdc-df83-4dda-8e26-1180714ac904",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAxe",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1c8f3786-c3f7-4a68-bfac-de5f03d6508b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca51dfdc-df83-4dda-8e26-1180714ac904",
            "compositeImage": {
                "id": "13fc8a8d-0cd3-42db-bf70-d6b6e9591d2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c8f3786-c3f7-4a68-bfac-de5f03d6508b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "623d808d-6662-4687-8e16-da15fe14a431",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c8f3786-c3f7-4a68-bfac-de5f03d6508b",
                    "LayerId": "1958748e-51d9-4a8d-b816-da86e202a7ad"
                }
            ]
        },
        {
            "id": "7dffe55a-e770-4a78-b53f-6a9c22dc5da2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca51dfdc-df83-4dda-8e26-1180714ac904",
            "compositeImage": {
                "id": "cddee16e-8470-4a0d-9eb5-330940314640",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dffe55a-e770-4a78-b53f-6a9c22dc5da2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7f42785-97c9-4ecd-9968-0b7596f45763",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dffe55a-e770-4a78-b53f-6a9c22dc5da2",
                    "LayerId": "1958748e-51d9-4a8d-b816-da86e202a7ad"
                }
            ]
        },
        {
            "id": "4d09fba5-b153-4b85-a4f5-d016d801139a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca51dfdc-df83-4dda-8e26-1180714ac904",
            "compositeImage": {
                "id": "bbb639f4-8a8f-4c98-b30e-6ea82494a8f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d09fba5-b153-4b85-a4f5-d016d801139a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "317dbeaa-d9b8-430f-86d7-f2b54763fc58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d09fba5-b153-4b85-a4f5-d016d801139a",
                    "LayerId": "1958748e-51d9-4a8d-b816-da86e202a7ad"
                }
            ]
        },
        {
            "id": "ca2566ff-bb24-4a6a-832e-eb2ffb248436",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca51dfdc-df83-4dda-8e26-1180714ac904",
            "compositeImage": {
                "id": "a6514881-7de8-4e86-837a-be4864dcda18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca2566ff-bb24-4a6a-832e-eb2ffb248436",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dbd8075-b2ce-4fe5-9076-77dfa8ff7661",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca2566ff-bb24-4a6a-832e-eb2ffb248436",
                    "LayerId": "1958748e-51d9-4a8d-b816-da86e202a7ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1958748e-51d9-4a8d-b816-da86e202a7ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca51dfdc-df83-4dda-8e26-1180714ac904",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}