{
    "id": "73932b6b-c231-4063-9938-ef25a3ac2804",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCross",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b9b94287-4c62-4848-b930-fd3cceb2d962",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73932b6b-c231-4063-9938-ef25a3ac2804",
            "compositeImage": {
                "id": "ee6f8633-c779-4f9f-abc7-4984d6a5b8fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9b94287-4c62-4848-b930-fd3cceb2d962",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ce421e1-ad7c-4b49-9d71-9638a608d45a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9b94287-4c62-4848-b930-fd3cceb2d962",
                    "LayerId": "34615895-95d5-4d67-aefa-b17faf43de5c"
                }
            ]
        },
        {
            "id": "f602ac80-155e-4e12-a988-fb5f0c845bba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73932b6b-c231-4063-9938-ef25a3ac2804",
            "compositeImage": {
                "id": "4b38081f-0844-4186-8827-f9a78ba8bdec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f602ac80-155e-4e12-a988-fb5f0c845bba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4b28bdc-49f6-477d-add6-d28a080305cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f602ac80-155e-4e12-a988-fb5f0c845bba",
                    "LayerId": "34615895-95d5-4d67-aefa-b17faf43de5c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "34615895-95d5-4d67-aefa-b17faf43de5c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73932b6b-c231-4063-9938-ef25a3ac2804",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}