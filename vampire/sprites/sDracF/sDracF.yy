{
    "id": "5d6eb084-3715-4d10-9d92-c748c5a5c2fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDracF",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "31ce7bcc-5b90-4426-9b6a-71adc6855062",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d6eb084-3715-4d10-9d92-c748c5a5c2fc",
            "compositeImage": {
                "id": "ebca37e4-0f36-4dce-9ff3-4807be511473",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31ce7bcc-5b90-4426-9b6a-71adc6855062",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f679f7fb-a2d1-4f0f-b88d-c07b76b2202f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31ce7bcc-5b90-4426-9b6a-71adc6855062",
                    "LayerId": "b9d433f0-aae4-4801-b14b-a333f0d76db0"
                }
            ]
        },
        {
            "id": "147bafdb-f56d-4240-a881-6afca93574ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d6eb084-3715-4d10-9d92-c748c5a5c2fc",
            "compositeImage": {
                "id": "61c9fe16-a8f6-43ba-93d7-20ebdb50906a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "147bafdb-f56d-4240-a881-6afca93574ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf4542db-61fb-4497-a7d3-01d4f68903e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "147bafdb-f56d-4240-a881-6afca93574ab",
                    "LayerId": "b9d433f0-aae4-4801-b14b-a333f0d76db0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b9d433f0-aae4-4801-b14b-a333f0d76db0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d6eb084-3715-4d10-9d92-c748c5a5c2fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 9,
    "yorig": 8
}