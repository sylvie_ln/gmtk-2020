{
    "id": "b1e26ff6-5cc6-4547-9c16-e4671f847d9b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sKnife",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9512da7-f63c-47be-b32c-fcef3cde97e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1e26ff6-5cc6-4547-9c16-e4671f847d9b",
            "compositeImage": {
                "id": "e0fc11b0-9231-4a87-bc89-c7a585780cce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9512da7-f63c-47be-b32c-fcef3cde97e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc094d16-76a6-4fab-b69d-a168590cbdc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9512da7-f63c-47be-b32c-fcef3cde97e8",
                    "LayerId": "b6108996-ee19-4084-b6dd-fb44888e470c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b6108996-ee19-4084-b6dd-fb44888e470c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1e26ff6-5cc6-4547-9c16-e4671f847d9b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}