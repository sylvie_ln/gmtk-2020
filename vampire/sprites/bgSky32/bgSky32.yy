{
    "id": "2f5e8e83-7f8f-4ac7-8d86-0471d65f74be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bgSky32",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "282a4e97-3859-4337-a12c-f14a72d69479",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f5e8e83-7f8f-4ac7-8d86-0471d65f74be",
            "compositeImage": {
                "id": "d0f22d09-780b-4297-9602-a61bd41018c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "282a4e97-3859-4337-a12c-f14a72d69479",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fea38217-5f5d-4086-ae15-b6d52e1c4094",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "282a4e97-3859-4337-a12c-f14a72d69479",
                    "LayerId": "fd843057-7570-4a16-ad3e-954afef10b38"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fd843057-7570-4a16-ad3e-954afef10b38",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2f5e8e83-7f8f-4ac7-8d86-0471d65f74be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}