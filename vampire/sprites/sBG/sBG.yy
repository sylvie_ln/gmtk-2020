{
    "id": "fb7eae9e-4eb2-4e3c-80f4-11538bcc14a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBG",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c4aa43e1-c489-4a5f-94c9-5f3512aeaece",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb7eae9e-4eb2-4e3c-80f4-11538bcc14a9",
            "compositeImage": {
                "id": "babe15e0-db23-4297-9163-cc0eb4e866f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4aa43e1-c489-4a5f-94c9-5f3512aeaece",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c756ab05-4e82-44f0-8806-d7358485f20a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4aa43e1-c489-4a5f-94c9-5f3512aeaece",
                    "LayerId": "1c4054c1-f120-4f2d-8ee2-d5f3c12670e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "1c4054c1-f120-4f2d-8ee2-d5f3c12670e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb7eae9e-4eb2-4e3c-80f4-11538bcc14a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 48
}