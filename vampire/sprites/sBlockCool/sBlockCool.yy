{
    "id": "df2f3677-2bae-4762-a390-3fa33505077a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBlockCool",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c2fb07d5-ee96-4445-afa2-96eabb0058f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df2f3677-2bae-4762-a390-3fa33505077a",
            "compositeImage": {
                "id": "d0a0ef25-84e2-49a0-9165-c765e83ba3b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2fb07d5-ee96-4445-afa2-96eabb0058f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23777752-07e2-4f50-9aa5-10e69803e5ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2fb07d5-ee96-4445-afa2-96eabb0058f6",
                    "LayerId": "878dd3de-6764-4990-8db3-6c5a19b92467"
                }
            ]
        },
        {
            "id": "b4ad1b32-2a22-477b-98bd-31aaddb8345d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df2f3677-2bae-4762-a390-3fa33505077a",
            "compositeImage": {
                "id": "3da34fda-d111-4cec-9c89-6c92a9583b90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4ad1b32-2a22-477b-98bd-31aaddb8345d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "526b3f45-4575-4a9d-805b-984d4b6370cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4ad1b32-2a22-477b-98bd-31aaddb8345d",
                    "LayerId": "878dd3de-6764-4990-8db3-6c5a19b92467"
                }
            ]
        },
        {
            "id": "789fbdfe-772c-4a36-88eb-1731ac2005f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df2f3677-2bae-4762-a390-3fa33505077a",
            "compositeImage": {
                "id": "0dbe2569-e861-405a-9b18-7c651072c743",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "789fbdfe-772c-4a36-88eb-1731ac2005f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a471071-e74f-4c81-a802-3eec6d4761b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "789fbdfe-772c-4a36-88eb-1731ac2005f2",
                    "LayerId": "878dd3de-6764-4990-8db3-6c5a19b92467"
                }
            ]
        },
        {
            "id": "0504c57f-349d-47c1-b61a-e0ffffc733c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df2f3677-2bae-4762-a390-3fa33505077a",
            "compositeImage": {
                "id": "35560f12-0516-4536-95e3-d0320938adbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0504c57f-349d-47c1-b61a-e0ffffc733c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "558cbed2-09b0-4e8f-a4fd-42d64b9438db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0504c57f-349d-47c1-b61a-e0ffffc733c9",
                    "LayerId": "878dd3de-6764-4990-8db3-6c5a19b92467"
                }
            ]
        },
        {
            "id": "8a282484-4bea-4b8a-a7a4-f2e498421102",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df2f3677-2bae-4762-a390-3fa33505077a",
            "compositeImage": {
                "id": "04b92e0d-060d-4f43-b80e-57e43a64efa8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a282484-4bea-4b8a-a7a4-f2e498421102",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17221831-f74c-4798-b24c-2c91ef740ba7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a282484-4bea-4b8a-a7a4-f2e498421102",
                    "LayerId": "878dd3de-6764-4990-8db3-6c5a19b92467"
                }
            ]
        },
        {
            "id": "ca3377e8-6170-4a45-899e-d18c1f18f3d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df2f3677-2bae-4762-a390-3fa33505077a",
            "compositeImage": {
                "id": "bf313c84-048e-42c7-8c2d-5a6b3b01af16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca3377e8-6170-4a45-899e-d18c1f18f3d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22d6f7ab-1ac4-4025-b3a2-f2e5636fd6d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca3377e8-6170-4a45-899e-d18c1f18f3d5",
                    "LayerId": "878dd3de-6764-4990-8db3-6c5a19b92467"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "878dd3de-6764-4990-8db3-6c5a19b92467",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df2f3677-2bae-4762-a390-3fa33505077a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}