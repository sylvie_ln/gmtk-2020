{
    "id": "4ee32e4e-0412-46eb-8266-fee7741af7f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMapp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9591928-c3ca-4ba4-a514-d93ff48060ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ee32e4e-0412-46eb-8266-fee7741af7f1",
            "compositeImage": {
                "id": "9c401303-8731-4f18-98e7-f5da8c0d363d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9591928-c3ca-4ba4-a514-d93ff48060ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62650941-3cd0-4ad8-a82a-f6eb6259d4d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9591928-c3ca-4ba4-a514-d93ff48060ba",
                    "LayerId": "0da6227c-f682-4ae4-b5c7-1b95b0a79e6c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "0da6227c-f682-4ae4-b5c7-1b95b0a79e6c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ee32e4e-0412-46eb-8266-fee7741af7f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}