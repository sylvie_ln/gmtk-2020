{
    "id": "db29d9f6-82f6-4c04-9114-6cdd19ce45ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sKitten",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b23fc05e-65c8-4324-933c-be8acda567cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db29d9f6-82f6-4c04-9114-6cdd19ce45ae",
            "compositeImage": {
                "id": "558691db-a32b-4b55-8bb1-0ade26a60612",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b23fc05e-65c8-4324-933c-be8acda567cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f076f3d1-cd37-432b-a888-ec889327cdc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b23fc05e-65c8-4324-933c-be8acda567cb",
                    "LayerId": "e0c06d33-8777-4939-afc9-99f20b9f053a"
                }
            ]
        },
        {
            "id": "6f8dce62-af56-4774-a5c5-2ca3de715ebf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db29d9f6-82f6-4c04-9114-6cdd19ce45ae",
            "compositeImage": {
                "id": "f5d6e327-e1c5-4d7a-b18d-f46c4f0853d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f8dce62-af56-4774-a5c5-2ca3de715ebf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a018288d-c55b-44dc-adaf-f40adc7ea851",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f8dce62-af56-4774-a5c5-2ca3de715ebf",
                    "LayerId": "e0c06d33-8777-4939-afc9-99f20b9f053a"
                }
            ]
        },
        {
            "id": "771af9ff-89e6-4bec-92c4-5ea6d1a15d6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db29d9f6-82f6-4c04-9114-6cdd19ce45ae",
            "compositeImage": {
                "id": "b5ab5bb5-35cf-462d-a6c0-189cda2c227b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "771af9ff-89e6-4bec-92c4-5ea6d1a15d6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "872aebae-f968-43f3-ab45-53001ba13bfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "771af9ff-89e6-4bec-92c4-5ea6d1a15d6c",
                    "LayerId": "e0c06d33-8777-4939-afc9-99f20b9f053a"
                }
            ]
        },
        {
            "id": "de79c249-9aa3-4168-9f66-497ce9fa854c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db29d9f6-82f6-4c04-9114-6cdd19ce45ae",
            "compositeImage": {
                "id": "da1da70b-88bd-4262-b25b-ccf9d457b743",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de79c249-9aa3-4168-9f66-497ce9fa854c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7db5dc9-0428-491e-9c4c-36db16edb2c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de79c249-9aa3-4168-9f66-497ce9fa854c",
                    "LayerId": "e0c06d33-8777-4939-afc9-99f20b9f053a"
                }
            ]
        },
        {
            "id": "f9fa0524-7767-4e25-b375-ee12b5c8a939",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db29d9f6-82f6-4c04-9114-6cdd19ce45ae",
            "compositeImage": {
                "id": "c28dad71-e150-4251-a33f-a669c8066934",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9fa0524-7767-4e25-b375-ee12b5c8a939",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37c5415a-a958-4b61-b143-083c114856cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9fa0524-7767-4e25-b375-ee12b5c8a939",
                    "LayerId": "e0c06d33-8777-4939-afc9-99f20b9f053a"
                }
            ]
        },
        {
            "id": "e1802c7c-3531-4195-960a-b8bd467ea033",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db29d9f6-82f6-4c04-9114-6cdd19ce45ae",
            "compositeImage": {
                "id": "8d0624d0-35eb-45a9-b899-fcd46affb09d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1802c7c-3531-4195-960a-b8bd467ea033",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b992bc9a-c346-41aa-a735-ee345874835a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1802c7c-3531-4195-960a-b8bd467ea033",
                    "LayerId": "e0c06d33-8777-4939-afc9-99f20b9f053a"
                }
            ]
        },
        {
            "id": "c25e74e8-138f-4a00-8080-4b059d6dabb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db29d9f6-82f6-4c04-9114-6cdd19ce45ae",
            "compositeImage": {
                "id": "b8d6a7ff-c794-44f6-898f-ef406fa91f35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c25e74e8-138f-4a00-8080-4b059d6dabb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cf7c008-ca93-4a10-a75b-7a54eb3a3abc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c25e74e8-138f-4a00-8080-4b059d6dabb5",
                    "LayerId": "e0c06d33-8777-4939-afc9-99f20b9f053a"
                }
            ]
        },
        {
            "id": "ef3574a1-fa80-49be-bcfa-ace1c74bfca6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db29d9f6-82f6-4c04-9114-6cdd19ce45ae",
            "compositeImage": {
                "id": "3f3ab433-8ef8-4318-8e6d-1ff66464e4be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef3574a1-fa80-49be-bcfa-ace1c74bfca6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c09b7ea7-fd26-434c-b601-928048e63a57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef3574a1-fa80-49be-bcfa-ace1c74bfca6",
                    "LayerId": "e0c06d33-8777-4939-afc9-99f20b9f053a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e0c06d33-8777-4939-afc9-99f20b9f053a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db29d9f6-82f6-4c04-9114-6cdd19ce45ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}