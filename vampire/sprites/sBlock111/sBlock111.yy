{
    "id": "1e44e096-bdd9-46c9-a138-a05398a13acd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBlock111",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff842bcf-4b6f-41fa-b322-f8ae3dd3fe97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e44e096-bdd9-46c9-a138-a05398a13acd",
            "compositeImage": {
                "id": "24feac17-60b6-467f-85f1-34ae5ac3f580",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff842bcf-4b6f-41fa-b322-f8ae3dd3fe97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bd41928-6303-428f-affc-76a9fb76a616",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff842bcf-4b6f-41fa-b322-f8ae3dd3fe97",
                    "LayerId": "e82756f1-3ff4-47c9-b82f-e80071fb8a8b"
                },
                {
                    "id": "bf9931d1-313b-4cfb-85d6-512acec09b90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff842bcf-4b6f-41fa-b322-f8ae3dd3fe97",
                    "LayerId": "3f6fcc0f-3778-46dd-a2e6-ce74602287d3"
                }
            ]
        },
        {
            "id": "6cf4c229-ae13-4b30-85e1-8309eab14e0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e44e096-bdd9-46c9-a138-a05398a13acd",
            "compositeImage": {
                "id": "949c5137-b920-4af4-b20b-00affe0123c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cf4c229-ae13-4b30-85e1-8309eab14e0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff373d22-de4b-43c6-8149-73c393ed014c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cf4c229-ae13-4b30-85e1-8309eab14e0f",
                    "LayerId": "e82756f1-3ff4-47c9-b82f-e80071fb8a8b"
                },
                {
                    "id": "10247e54-f78c-4294-8d83-f836584d82d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cf4c229-ae13-4b30-85e1-8309eab14e0f",
                    "LayerId": "3f6fcc0f-3778-46dd-a2e6-ce74602287d3"
                }
            ]
        },
        {
            "id": "2c74b082-64ad-409f-9889-5b5fdedc3eb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e44e096-bdd9-46c9-a138-a05398a13acd",
            "compositeImage": {
                "id": "67843cbf-e448-4a70-8022-6861adc91a6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c74b082-64ad-409f-9889-5b5fdedc3eb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39a74407-7c04-4481-a7b3-41543ac8cd85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c74b082-64ad-409f-9889-5b5fdedc3eb6",
                    "LayerId": "e82756f1-3ff4-47c9-b82f-e80071fb8a8b"
                },
                {
                    "id": "b3d73ab5-c28e-42f5-ad8d-babad2123aa5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c74b082-64ad-409f-9889-5b5fdedc3eb6",
                    "LayerId": "3f6fcc0f-3778-46dd-a2e6-ce74602287d3"
                }
            ]
        },
        {
            "id": "30f02883-6012-4c1e-91b2-daec3c3d1b5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e44e096-bdd9-46c9-a138-a05398a13acd",
            "compositeImage": {
                "id": "14d56152-2749-4667-89b6-491f53b013e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30f02883-6012-4c1e-91b2-daec3c3d1b5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e46649f-d743-409b-a181-808765cee791",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30f02883-6012-4c1e-91b2-daec3c3d1b5a",
                    "LayerId": "e82756f1-3ff4-47c9-b82f-e80071fb8a8b"
                },
                {
                    "id": "642ad2eb-2cb8-4b21-bf65-d03ca4dbfd8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30f02883-6012-4c1e-91b2-daec3c3d1b5a",
                    "LayerId": "3f6fcc0f-3778-46dd-a2e6-ce74602287d3"
                }
            ]
        },
        {
            "id": "228eccca-411b-4e89-b1bc-56b672d16dc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e44e096-bdd9-46c9-a138-a05398a13acd",
            "compositeImage": {
                "id": "468fe17f-4080-4349-b7e6-7451408a08ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "228eccca-411b-4e89-b1bc-56b672d16dc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f65ada4b-65b6-4114-9c26-9d4934f61365",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "228eccca-411b-4e89-b1bc-56b672d16dc7",
                    "LayerId": "e82756f1-3ff4-47c9-b82f-e80071fb8a8b"
                },
                {
                    "id": "ff1c0a9d-a617-4aa0-9328-b191a78c1bde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "228eccca-411b-4e89-b1bc-56b672d16dc7",
                    "LayerId": "3f6fcc0f-3778-46dd-a2e6-ce74602287d3"
                }
            ]
        },
        {
            "id": "5d60247b-3611-4f14-862e-f7a28eff1664",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e44e096-bdd9-46c9-a138-a05398a13acd",
            "compositeImage": {
                "id": "49167411-76a0-48b5-b771-8ec7a298bfd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d60247b-3611-4f14-862e-f7a28eff1664",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd31f08e-0576-4f7e-bc96-c9952dc0a935",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d60247b-3611-4f14-862e-f7a28eff1664",
                    "LayerId": "e82756f1-3ff4-47c9-b82f-e80071fb8a8b"
                },
                {
                    "id": "ae873786-2bbc-49ae-a563-f648a21a750e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d60247b-3611-4f14-862e-f7a28eff1664",
                    "LayerId": "3f6fcc0f-3778-46dd-a2e6-ce74602287d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3f6fcc0f-3778-46dd-a2e6-ce74602287d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e44e096-bdd9-46c9-a138-a05398a13acd",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "e82756f1-3ff4-47c9-b82f-e80071fb8a8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e44e096-bdd9-46c9-a138-a05398a13acd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}