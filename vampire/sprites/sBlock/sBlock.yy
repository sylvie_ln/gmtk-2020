{
    "id": "1c40e3d6-5991-4ff2-baef-f9a5fd1a6459",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBlock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9591f31f-3a1b-4325-82e8-cf737f66eee5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c40e3d6-5991-4ff2-baef-f9a5fd1a6459",
            "compositeImage": {
                "id": "88fcc06f-a5ae-4f9f-8197-f5e1bf202f3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9591f31f-3a1b-4325-82e8-cf737f66eee5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8677cde-8406-4fa1-8204-5d0dea7cf939",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9591f31f-3a1b-4325-82e8-cf737f66eee5",
                    "LayerId": "b11ccea5-b4e3-4d92-bb58-4f9e59e47d9f"
                }
            ]
        },
        {
            "id": "d58e7716-e316-4749-bef1-f78d4b4ac7c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c40e3d6-5991-4ff2-baef-f9a5fd1a6459",
            "compositeImage": {
                "id": "44fc3e7a-5f93-400a-811d-4738d7f1e151",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d58e7716-e316-4749-bef1-f78d4b4ac7c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b37ad1c-a952-4b73-a7ff-91038f17cabf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d58e7716-e316-4749-bef1-f78d4b4ac7c5",
                    "LayerId": "b11ccea5-b4e3-4d92-bb58-4f9e59e47d9f"
                }
            ]
        },
        {
            "id": "2a3a9ebe-e1c5-40d0-9f87-80f5621af9e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c40e3d6-5991-4ff2-baef-f9a5fd1a6459",
            "compositeImage": {
                "id": "4551e277-a96a-4e70-b51a-b308b775b50f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a3a9ebe-e1c5-40d0-9f87-80f5621af9e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9d1b5f1-4c80-4870-9c02-f8db1496d547",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a3a9ebe-e1c5-40d0-9f87-80f5621af9e1",
                    "LayerId": "b11ccea5-b4e3-4d92-bb58-4f9e59e47d9f"
                }
            ]
        },
        {
            "id": "ba23ff09-a162-42e7-959c-0fc60f91f3f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c40e3d6-5991-4ff2-baef-f9a5fd1a6459",
            "compositeImage": {
                "id": "5693359f-92f7-439d-9947-9a33a510810b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba23ff09-a162-42e7-959c-0fc60f91f3f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6094e6fb-0c3f-4a5b-ba7e-3d40cf2da54a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba23ff09-a162-42e7-959c-0fc60f91f3f3",
                    "LayerId": "b11ccea5-b4e3-4d92-bb58-4f9e59e47d9f"
                }
            ]
        },
        {
            "id": "7717b2e2-adf5-440b-a926-b9d31482f16f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c40e3d6-5991-4ff2-baef-f9a5fd1a6459",
            "compositeImage": {
                "id": "2b1cbef4-73ca-426d-8a58-c75b43f683ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7717b2e2-adf5-440b-a926-b9d31482f16f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d7c3369-3b95-4033-a5a2-70d63be91473",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7717b2e2-adf5-440b-a926-b9d31482f16f",
                    "LayerId": "b11ccea5-b4e3-4d92-bb58-4f9e59e47d9f"
                }
            ]
        },
        {
            "id": "614d5258-a2fd-4dd3-9e9f-817dba9483f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c40e3d6-5991-4ff2-baef-f9a5fd1a6459",
            "compositeImage": {
                "id": "80b5ae4a-29eb-4880-9131-fbb64a43f62a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "614d5258-a2fd-4dd3-9e9f-817dba9483f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1680f2fb-dab7-4ee0-8c55-13e00880179b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "614d5258-a2fd-4dd3-9e9f-817dba9483f2",
                    "LayerId": "b11ccea5-b4e3-4d92-bb58-4f9e59e47d9f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b11ccea5-b4e3-4d92-bb58-4f9e59e47d9f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c40e3d6-5991-4ff2-baef-f9a5fd1a6459",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}