{
    "id": "a6d90d88-667c-4c9f-93d1-664a43f5c127",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDeath",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3acd1893-1f2a-4c1d-b3fa-f7c765a7c901",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6d90d88-667c-4c9f-93d1-664a43f5c127",
            "compositeImage": {
                "id": "d183782f-49a7-401b-b136-1365948f0034",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3acd1893-1f2a-4c1d-b3fa-f7c765a7c901",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "621a8b95-3e12-4c2f-98b3-b7494fa7b5f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3acd1893-1f2a-4c1d-b3fa-f7c765a7c901",
                    "LayerId": "828f335c-e839-4d07-8a65-ff4844cdb398"
                }
            ]
        },
        {
            "id": "516c7740-e574-4701-81b3-39950ebef18a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6d90d88-667c-4c9f-93d1-664a43f5c127",
            "compositeImage": {
                "id": "1ee6adff-e39f-4c01-bfbd-669a2540ab23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "516c7740-e574-4701-81b3-39950ebef18a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7579839e-03e1-4166-9e7d-8982c957308d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "516c7740-e574-4701-81b3-39950ebef18a",
                    "LayerId": "828f335c-e839-4d07-8a65-ff4844cdb398"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "828f335c-e839-4d07-8a65-ff4844cdb398",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6d90d88-667c-4c9f-93d1-664a43f5c127",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}