{
    "id": "41554f7f-4214-481b-8d4a-68401f9f3b19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sKittenBat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3e134a7-b4c1-4669-8647-5700f273c598",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41554f7f-4214-481b-8d4a-68401f9f3b19",
            "compositeImage": {
                "id": "12b637c2-23a1-4751-b6ee-d80d354ff2ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3e134a7-b4c1-4669-8647-5700f273c598",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3ec9281-020a-4346-b9fe-2400bb687c40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3e134a7-b4c1-4669-8647-5700f273c598",
                    "LayerId": "48eec42e-e033-4369-8c74-55fc580b265f"
                }
            ]
        },
        {
            "id": "4b113723-5ca3-435a-829d-151f9a7699c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41554f7f-4214-481b-8d4a-68401f9f3b19",
            "compositeImage": {
                "id": "558eb607-cf12-49cd-93b1-73d729972806",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b113723-5ca3-435a-829d-151f9a7699c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6a058e3-590a-4d6b-a754-0d3167e0df08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b113723-5ca3-435a-829d-151f9a7699c4",
                    "LayerId": "48eec42e-e033-4369-8c74-55fc580b265f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "48eec42e-e033-4369-8c74-55fc580b265f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "41554f7f-4214-481b-8d4a-68401f9f3b19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}