{
    "id": "f1dfe9a5-cae7-43b1-9f8f-5e1a86ae1fef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDracR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d7ca9e45-5d08-48d7-b27d-7e7aaacc43b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1dfe9a5-cae7-43b1-9f8f-5e1a86ae1fef",
            "compositeImage": {
                "id": "8664b1e2-69d2-4a6a-bf7f-76ce71c9fb09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7ca9e45-5d08-48d7-b27d-7e7aaacc43b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75fc5e35-6d67-4644-b3a3-805d85aa4fcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7ca9e45-5d08-48d7-b27d-7e7aaacc43b3",
                    "LayerId": "a6f948f6-1aa7-446c-b763-8259accdc9eb"
                }
            ]
        },
        {
            "id": "09a0e19f-04f6-49e4-9e20-36f8bcb1d261",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1dfe9a5-cae7-43b1-9f8f-5e1a86ae1fef",
            "compositeImage": {
                "id": "411e151d-2d71-4ae3-8e19-0e09fd4536c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09a0e19f-04f6-49e4-9e20-36f8bcb1d261",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76f25dc9-621b-446c-a94e-3ac74f9372ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09a0e19f-04f6-49e4-9e20-36f8bcb1d261",
                    "LayerId": "a6f948f6-1aa7-446c-b763-8259accdc9eb"
                }
            ]
        },
        {
            "id": "6fe25dbb-b2bd-4825-84df-b0d78dd83ab3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1dfe9a5-cae7-43b1-9f8f-5e1a86ae1fef",
            "compositeImage": {
                "id": "6c50e319-243d-4b60-a8e3-9d5b221bc03c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fe25dbb-b2bd-4825-84df-b0d78dd83ab3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92771cab-db7d-4aaf-bc1a-952aac484a94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fe25dbb-b2bd-4825-84df-b0d78dd83ab3",
                    "LayerId": "a6f948f6-1aa7-446c-b763-8259accdc9eb"
                }
            ]
        },
        {
            "id": "c7a9fd3e-f8af-4610-aef1-5b47d1bed055",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1dfe9a5-cae7-43b1-9f8f-5e1a86ae1fef",
            "compositeImage": {
                "id": "7000df02-9458-4962-9b0e-52770435afd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7a9fd3e-f8af-4610-aef1-5b47d1bed055",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6b3155d-39e7-4a05-b794-6702c8616005",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7a9fd3e-f8af-4610-aef1-5b47d1bed055",
                    "LayerId": "a6f948f6-1aa7-446c-b763-8259accdc9eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a6f948f6-1aa7-446c-b763-8259accdc9eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1dfe9a5-cae7-43b1-9f8f-5e1a86ae1fef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 9,
    "yorig": 8
}