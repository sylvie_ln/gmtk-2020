{
    "id": "fb70aec5-b4eb-429d-8cc6-9deac74e1069",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sKittenTrigger",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b022f4e-5b2e-4d1c-8abd-5bbbdcf00395",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb70aec5-b4eb-429d-8cc6-9deac74e1069",
            "compositeImage": {
                "id": "127dd713-92bd-4045-882d-a72c27123ec6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b022f4e-5b2e-4d1c-8abd-5bbbdcf00395",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "274c7e10-1969-40ab-b7f7-70d2ee897f19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b022f4e-5b2e-4d1c-8abd-5bbbdcf00395",
                    "LayerId": "891692ed-f06d-4114-9365-7d05fa8c8d47"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "891692ed-f06d-4114-9365-7d05fa8c8d47",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb70aec5-b4eb-429d-8cc6-9deac74e1069",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 50,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}