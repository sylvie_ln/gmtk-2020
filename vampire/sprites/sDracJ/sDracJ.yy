{
    "id": "dfa01eb8-85a4-4979-a851-b233bb4bc888",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDracJ",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb1a1b69-b77f-4a4f-bfbb-1f46105f62e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfa01eb8-85a4-4979-a851-b233bb4bc888",
            "compositeImage": {
                "id": "7f857cc6-cb6e-4004-a603-9cdb23a86cd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb1a1b69-b77f-4a4f-bfbb-1f46105f62e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b1b2d26-ae80-47ef-ac40-45d1e0eebe10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb1a1b69-b77f-4a4f-bfbb-1f46105f62e1",
                    "LayerId": "0f507b36-af2d-483b-8585-24f8c2aa06b3"
                }
            ]
        },
        {
            "id": "2ccc5c21-25a3-43b5-bd69-e64ac68faaa9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfa01eb8-85a4-4979-a851-b233bb4bc888",
            "compositeImage": {
                "id": "c2fab1e0-a113-42ee-b16d-7a34469ff80e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ccc5c21-25a3-43b5-bd69-e64ac68faaa9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48d9cea7-e9a9-45c4-9d8f-0da5630dd568",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ccc5c21-25a3-43b5-bd69-e64ac68faaa9",
                    "LayerId": "0f507b36-af2d-483b-8585-24f8c2aa06b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0f507b36-af2d-483b-8585-24f8c2aa06b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dfa01eb8-85a4-4979-a851-b233bb4bc888",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 9,
    "yorig": 8
}