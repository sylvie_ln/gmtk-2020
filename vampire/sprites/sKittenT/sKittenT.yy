{
    "id": "24de98ef-6b01-4ed0-8c94-2579e60074eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sKittenT",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d4e9798f-487d-4220-80da-5013cfe68e91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24de98ef-6b01-4ed0-8c94-2579e60074eb",
            "compositeImage": {
                "id": "34383e4b-4dee-4c2f-9642-3e0848d8553a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4e9798f-487d-4220-80da-5013cfe68e91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6003e5b1-4afc-44d6-a095-26299975fa51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4e9798f-487d-4220-80da-5013cfe68e91",
                    "LayerId": "e612cdb5-9078-4dd8-9632-fb32b7c20ded"
                }
            ]
        },
        {
            "id": "b91f9665-f2c0-4b0d-a42b-94bbdabf0987",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24de98ef-6b01-4ed0-8c94-2579e60074eb",
            "compositeImage": {
                "id": "d580bbe0-9a34-457b-88d9-ca97ab4bfa2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b91f9665-f2c0-4b0d-a42b-94bbdabf0987",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf49911a-6dbe-4544-ada8-9a6934be0d90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b91f9665-f2c0-4b0d-a42b-94bbdabf0987",
                    "LayerId": "e612cdb5-9078-4dd8-9632-fb32b7c20ded"
                }
            ]
        },
        {
            "id": "b646da46-c1f3-40bf-831d-4b6633db19c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24de98ef-6b01-4ed0-8c94-2579e60074eb",
            "compositeImage": {
                "id": "16e6dd66-2d97-404e-b098-05e4d493db4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b646da46-c1f3-40bf-831d-4b6633db19c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27957ee3-f403-4ac1-b070-d0d8fcaaea2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b646da46-c1f3-40bf-831d-4b6633db19c4",
                    "LayerId": "e612cdb5-9078-4dd8-9632-fb32b7c20ded"
                }
            ]
        },
        {
            "id": "3fee889a-e9a4-46be-8008-93f09d8d9655",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24de98ef-6b01-4ed0-8c94-2579e60074eb",
            "compositeImage": {
                "id": "3fe2ec95-f967-4856-8831-6a48d92cf0da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fee889a-e9a4-46be-8008-93f09d8d9655",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49e4ac03-52f5-4024-b76e-4c5765c04d30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fee889a-e9a4-46be-8008-93f09d8d9655",
                    "LayerId": "e612cdb5-9078-4dd8-9632-fb32b7c20ded"
                }
            ]
        },
        {
            "id": "d68e362f-0aae-4028-9379-31d1753a3ad6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24de98ef-6b01-4ed0-8c94-2579e60074eb",
            "compositeImage": {
                "id": "6db8826f-a00a-461a-8687-1fcd4e68058a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d68e362f-0aae-4028-9379-31d1753a3ad6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af6a3db6-dec8-4759-89b1-431cc92f6956",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d68e362f-0aae-4028-9379-31d1753a3ad6",
                    "LayerId": "e612cdb5-9078-4dd8-9632-fb32b7c20ded"
                }
            ]
        },
        {
            "id": "66dcc3df-ae3d-4cf1-b180-e72af4fa0e90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24de98ef-6b01-4ed0-8c94-2579e60074eb",
            "compositeImage": {
                "id": "d270bc11-1f94-4c62-8433-e780ed1e81c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66dcc3df-ae3d-4cf1-b180-e72af4fa0e90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8e9ab08-374c-4e04-be0a-c0bb2de28499",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66dcc3df-ae3d-4cf1-b180-e72af4fa0e90",
                    "LayerId": "e612cdb5-9078-4dd8-9632-fb32b7c20ded"
                }
            ]
        },
        {
            "id": "19f9d795-cbe8-4467-9827-cfc141bafb19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24de98ef-6b01-4ed0-8c94-2579e60074eb",
            "compositeImage": {
                "id": "52a15505-3116-46d3-8a8e-8dfd6a37b09f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19f9d795-cbe8-4467-9827-cfc141bafb19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f33ea89a-2573-4e43-8111-0df45f6768b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19f9d795-cbe8-4467-9827-cfc141bafb19",
                    "LayerId": "e612cdb5-9078-4dd8-9632-fb32b7c20ded"
                }
            ]
        },
        {
            "id": "2b38254b-aed5-478e-99a6-63212b14dd88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24de98ef-6b01-4ed0-8c94-2579e60074eb",
            "compositeImage": {
                "id": "19c789a9-66a0-47e4-ba3a-33bcfdd64e98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b38254b-aed5-478e-99a6-63212b14dd88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "633f2310-a893-4d90-aee0-5ae18d24cf25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b38254b-aed5-478e-99a6-63212b14dd88",
                    "LayerId": "e612cdb5-9078-4dd8-9632-fb32b7c20ded"
                }
            ]
        },
        {
            "id": "1174077b-ec2e-436f-9b12-f87bbb9dddc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24de98ef-6b01-4ed0-8c94-2579e60074eb",
            "compositeImage": {
                "id": "441eb3ec-dcae-4563-b49a-05077f74ad47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1174077b-ec2e-436f-9b12-f87bbb9dddc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00b2a7d7-9ff4-43f1-a6da-2028d961ade8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1174077b-ec2e-436f-9b12-f87bbb9dddc0",
                    "LayerId": "e612cdb5-9078-4dd8-9632-fb32b7c20ded"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e612cdb5-9078-4dd8-9632-fb32b7c20ded",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "24de98ef-6b01-4ed0-8c94-2579e60074eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}