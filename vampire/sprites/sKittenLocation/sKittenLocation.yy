{
    "id": "4e7a0e28-d4de-4211-84f6-b1b6ebddcc9e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sKittenLocation",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21c2f1dc-c1dd-47c1-adb9-c72d4ba4c6ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e7a0e28-d4de-4211-84f6-b1b6ebddcc9e",
            "compositeImage": {
                "id": "195b090b-3259-4ca2-ab94-359f836c0d67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21c2f1dc-c1dd-47c1-adb9-c72d4ba4c6ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13a98447-111f-4ca9-9f70-6228b2ad6bea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21c2f1dc-c1dd-47c1-adb9-c72d4ba4c6ec",
                    "LayerId": "21474498-77a0-4708-9188-286a5d6ef697"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "21474498-77a0-4708-9188-286a5d6ef697",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e7a0e28-d4de-4211-84f6-b1b6ebddcc9e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 50,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}