{
    "id": "72496fd6-d60e-4c98-bffd-eb78771808db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bgSky",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f71211db-d806-4167-ad8d-3c80e82d5fd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72496fd6-d60e-4c98-bffd-eb78771808db",
            "compositeImage": {
                "id": "b158db02-107c-4fee-9c42-69de71ae5f79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f71211db-d806-4167-ad8d-3c80e82d5fd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13f76c89-5182-4bfd-91ed-556a95a72d1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f71211db-d806-4167-ad8d-3c80e82d5fd9",
                    "LayerId": "bac91533-b0e4-4858-b0c2-1fc53702f603"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "bac91533-b0e4-4858-b0c2-1fc53702f603",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72496fd6-d60e-4c98-bffd-eb78771808db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}