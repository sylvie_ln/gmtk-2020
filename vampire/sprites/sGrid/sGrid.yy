{
    "id": "47eb71b4-16a3-4c64-b03b-fe4ec685ca86",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGrid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9e4af5b-3464-4c76-b6db-f3c41d1ce81a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47eb71b4-16a3-4c64-b03b-fe4ec685ca86",
            "compositeImage": {
                "id": "5e1fbf84-e0dc-4c62-9ed2-a6b2e7061e10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9e4af5b-3464-4c76-b6db-f3c41d1ce81a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0d043f0-0b4a-4448-9894-caba4deac00f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9e4af5b-3464-4c76-b6db-f3c41d1ce81a",
                    "LayerId": "6764e335-a228-4698-bf95-fb8708eea16a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "6764e335-a228-4698-bf95-fb8708eea16a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47eb71b4-16a3-4c64-b03b-fe4ec685ca86",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 50,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 80
}