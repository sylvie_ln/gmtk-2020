{
    "id": "49373ae9-8dd2-4e15-84ce-ed0313ff75b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTackle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "44073c51-99a7-4d5a-8fed-87087d8afaef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49373ae9-8dd2-4e15-84ce-ed0313ff75b9",
            "compositeImage": {
                "id": "242a31ef-2ebf-44c1-9ba1-f1a7f93383fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44073c51-99a7-4d5a-8fed-87087d8afaef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a94bcafa-094b-40fc-89a1-9b89324452be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44073c51-99a7-4d5a-8fed-87087d8afaef",
                    "LayerId": "0732b857-d533-4922-9138-25e0ae0de53f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0732b857-d533-4922-9138-25e0ae0de53f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "49373ae9-8dd2-4e15-84ce-ed0313ff75b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 22,
    "yorig": 16
}