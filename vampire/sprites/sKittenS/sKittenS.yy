{
    "id": "e056d818-f37e-4e47-a35b-194cf5cd283b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sKittenS",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9289863e-1d95-42da-99bb-5ab17dbe7fbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e056d818-f37e-4e47-a35b-194cf5cd283b",
            "compositeImage": {
                "id": "be1b2082-97a6-4ca4-91a0-1d62702b7f7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9289863e-1d95-42da-99bb-5ab17dbe7fbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df3a1da0-7afe-4a07-951e-7e88650c2a38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9289863e-1d95-42da-99bb-5ab17dbe7fbd",
                    "LayerId": "0784b502-c17c-4965-afca-a386d1528e69"
                }
            ]
        },
        {
            "id": "1b148a65-2f98-453c-8242-764763ddcc7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e056d818-f37e-4e47-a35b-194cf5cd283b",
            "compositeImage": {
                "id": "e6044648-80fc-4859-9686-45614f0b3d4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b148a65-2f98-453c-8242-764763ddcc7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c1d3bc7-27c1-4336-974d-ff864df841d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b148a65-2f98-453c-8242-764763ddcc7e",
                    "LayerId": "0784b502-c17c-4965-afca-a386d1528e69"
                }
            ]
        },
        {
            "id": "1aa2ff04-4fe4-4cf9-86b0-8c41c45f0303",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e056d818-f37e-4e47-a35b-194cf5cd283b",
            "compositeImage": {
                "id": "6ab31f16-aa57-47e7-aced-59016f577a0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1aa2ff04-4fe4-4cf9-86b0-8c41c45f0303",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab0752bb-80ca-4e31-a1f7-353b5d3aaa9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1aa2ff04-4fe4-4cf9-86b0-8c41c45f0303",
                    "LayerId": "0784b502-c17c-4965-afca-a386d1528e69"
                }
            ]
        },
        {
            "id": "3fb41225-9d9a-4fb4-aa64-830a087cb20f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e056d818-f37e-4e47-a35b-194cf5cd283b",
            "compositeImage": {
                "id": "ff162613-86ed-49ef-ad12-e23a1ec7e5b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fb41225-9d9a-4fb4-aa64-830a087cb20f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b8591fd-19ca-4d4c-b1ea-9c8fd54b67b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fb41225-9d9a-4fb4-aa64-830a087cb20f",
                    "LayerId": "0784b502-c17c-4965-afca-a386d1528e69"
                }
            ]
        },
        {
            "id": "3a2d0574-3acb-4624-bb2f-9c7dd64fb688",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e056d818-f37e-4e47-a35b-194cf5cd283b",
            "compositeImage": {
                "id": "cfe3fd03-acd8-47bc-852b-19909b5d0bb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a2d0574-3acb-4624-bb2f-9c7dd64fb688",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "838b51ba-cc5d-4a84-8e1e-796bc106c3a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a2d0574-3acb-4624-bb2f-9c7dd64fb688",
                    "LayerId": "0784b502-c17c-4965-afca-a386d1528e69"
                }
            ]
        },
        {
            "id": "8fcff397-86d6-4ab8-965c-5e6b160ae6b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e056d818-f37e-4e47-a35b-194cf5cd283b",
            "compositeImage": {
                "id": "c83ecfba-50f6-49a4-884f-12dedb6a6907",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fcff397-86d6-4ab8-965c-5e6b160ae6b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d02c57b1-069f-4025-8dbd-c1a10fe1034f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fcff397-86d6-4ab8-965c-5e6b160ae6b4",
                    "LayerId": "0784b502-c17c-4965-afca-a386d1528e69"
                }
            ]
        },
        {
            "id": "1db8ada1-4283-4965-b3c6-cb44f091d037",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e056d818-f37e-4e47-a35b-194cf5cd283b",
            "compositeImage": {
                "id": "395f7a37-1918-430d-82a8-ea1d133dbbe2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1db8ada1-4283-4965-b3c6-cb44f091d037",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27327d2f-68de-40a9-ac19-394185fffb02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1db8ada1-4283-4965-b3c6-cb44f091d037",
                    "LayerId": "0784b502-c17c-4965-afca-a386d1528e69"
                }
            ]
        },
        {
            "id": "8b938825-be61-4644-9c3d-b0f1064c7a3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e056d818-f37e-4e47-a35b-194cf5cd283b",
            "compositeImage": {
                "id": "af2025d3-103a-4460-939e-f1930c3835ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b938825-be61-4644-9c3d-b0f1064c7a3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b67941f-7fc1-4e22-aaeb-fb3144eee578",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b938825-be61-4644-9c3d-b0f1064c7a3b",
                    "LayerId": "0784b502-c17c-4965-afca-a386d1528e69"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0784b502-c17c-4965-afca-a386d1528e69",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e056d818-f37e-4e47-a35b-194cf5cd283b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}