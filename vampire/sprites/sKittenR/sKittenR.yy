{
    "id": "83dea948-e4e5-460d-a5be-19061f960c73",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sKittenR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dcd4c934-649e-49e6-8163-e1fe10d254b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83dea948-e4e5-460d-a5be-19061f960c73",
            "compositeImage": {
                "id": "53d92a5f-6f85-431d-806f-ce328fcb4e29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcd4c934-649e-49e6-8163-e1fe10d254b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0892d49-aaed-4861-9433-3cbfabb5f439",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcd4c934-649e-49e6-8163-e1fe10d254b7",
                    "LayerId": "728ce7ea-9d73-413a-8c81-4127847f8ca2"
                }
            ]
        },
        {
            "id": "dba49312-b8fa-4d20-af6c-21e4b0cb4d55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83dea948-e4e5-460d-a5be-19061f960c73",
            "compositeImage": {
                "id": "8647c137-7f42-47b7-a074-1671fba91b6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dba49312-b8fa-4d20-af6c-21e4b0cb4d55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75f21706-a46e-4728-ada8-55b49b758676",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dba49312-b8fa-4d20-af6c-21e4b0cb4d55",
                    "LayerId": "728ce7ea-9d73-413a-8c81-4127847f8ca2"
                }
            ]
        },
        {
            "id": "62b0afd7-f2ae-4409-9598-0c5e634cd341",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83dea948-e4e5-460d-a5be-19061f960c73",
            "compositeImage": {
                "id": "2fe9acd4-fa64-4cf4-a103-055d206576e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62b0afd7-f2ae-4409-9598-0c5e634cd341",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2945857d-e4b4-4ab9-bff6-dd704da2b6a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62b0afd7-f2ae-4409-9598-0c5e634cd341",
                    "LayerId": "728ce7ea-9d73-413a-8c81-4127847f8ca2"
                }
            ]
        },
        {
            "id": "ca535e0c-6170-47dd-9341-ca4e43824357",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83dea948-e4e5-460d-a5be-19061f960c73",
            "compositeImage": {
                "id": "5988708a-0250-47ab-8062-9bb6d1126072",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca535e0c-6170-47dd-9341-ca4e43824357",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64f6dd67-a0d5-48e7-8479-d90a90ba0273",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca535e0c-6170-47dd-9341-ca4e43824357",
                    "LayerId": "728ce7ea-9d73-413a-8c81-4127847f8ca2"
                }
            ]
        },
        {
            "id": "be3d4e3e-817c-459a-a0b0-34582a2e97c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83dea948-e4e5-460d-a5be-19061f960c73",
            "compositeImage": {
                "id": "28920138-ba9e-40b1-b45c-76a718757282",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be3d4e3e-817c-459a-a0b0-34582a2e97c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30e57081-e513-4ff9-af4f-f9b7ba49f5e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be3d4e3e-817c-459a-a0b0-34582a2e97c0",
                    "LayerId": "728ce7ea-9d73-413a-8c81-4127847f8ca2"
                }
            ]
        },
        {
            "id": "3e6a4a05-c577-4bb5-b691-23e5aa8a2e56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83dea948-e4e5-460d-a5be-19061f960c73",
            "compositeImage": {
                "id": "1be594ba-1af7-4c34-9a8c-a2f08da4b0c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e6a4a05-c577-4bb5-b691-23e5aa8a2e56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd6210e5-3557-46e0-b891-02d71fd74f4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e6a4a05-c577-4bb5-b691-23e5aa8a2e56",
                    "LayerId": "728ce7ea-9d73-413a-8c81-4127847f8ca2"
                }
            ]
        },
        {
            "id": "bf4e68a2-90ec-4b17-b3a9-043c874570da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83dea948-e4e5-460d-a5be-19061f960c73",
            "compositeImage": {
                "id": "e40678aa-d27c-464a-bfbb-98020c51cce2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf4e68a2-90ec-4b17-b3a9-043c874570da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b654546-9e6c-481c-869f-f397785269eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf4e68a2-90ec-4b17-b3a9-043c874570da",
                    "LayerId": "728ce7ea-9d73-413a-8c81-4127847f8ca2"
                }
            ]
        },
        {
            "id": "4a7e0beb-4fd0-4f4d-816b-f330258f935b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83dea948-e4e5-460d-a5be-19061f960c73",
            "compositeImage": {
                "id": "942e5d5e-9a8d-4875-9e72-2ddb2a695ee1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a7e0beb-4fd0-4f4d-816b-f330258f935b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdc3a4fc-8127-4cc3-a186-8ee436da4321",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a7e0beb-4fd0-4f4d-816b-f330258f935b",
                    "LayerId": "728ce7ea-9d73-413a-8c81-4127847f8ca2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "728ce7ea-9d73-413a-8c81-4127847f8ca2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83dea948-e4e5-460d-a5be-19061f960c73",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}