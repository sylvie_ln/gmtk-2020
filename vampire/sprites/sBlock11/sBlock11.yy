{
    "id": "bff61b7d-c694-4d6d-8c65-77603db4d943",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBlock11",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "47e42962-be97-4a3f-aa3d-5e408b600768",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff61b7d-c694-4d6d-8c65-77603db4d943",
            "compositeImage": {
                "id": "6f5e8c37-613b-4a4f-9701-9c8ae4cfa0c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47e42962-be97-4a3f-aa3d-5e408b600768",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "197a7b03-6ef9-4eda-88ac-88a9f92d3a06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47e42962-be97-4a3f-aa3d-5e408b600768",
                    "LayerId": "3e0e3cde-dcb8-4682-8f1c-b0b847b7ac03"
                }
            ]
        },
        {
            "id": "d5d74c5f-7f16-44fa-bfb5-42054da26761",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff61b7d-c694-4d6d-8c65-77603db4d943",
            "compositeImage": {
                "id": "326f6bd5-84ea-4532-b763-c711d6495503",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5d74c5f-7f16-44fa-bfb5-42054da26761",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f84c4c53-cdc2-4c61-bb96-f4ffc3f3602a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5d74c5f-7f16-44fa-bfb5-42054da26761",
                    "LayerId": "3e0e3cde-dcb8-4682-8f1c-b0b847b7ac03"
                }
            ]
        },
        {
            "id": "66033068-83de-491d-b10c-8f3b7d9699b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff61b7d-c694-4d6d-8c65-77603db4d943",
            "compositeImage": {
                "id": "64f80d0c-fd68-4b30-bcfe-3ca992bdb5a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66033068-83de-491d-b10c-8f3b7d9699b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "158002b7-a492-4c40-b903-872c16db2979",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66033068-83de-491d-b10c-8f3b7d9699b5",
                    "LayerId": "3e0e3cde-dcb8-4682-8f1c-b0b847b7ac03"
                }
            ]
        },
        {
            "id": "d638c5b6-2bd3-4be6-a374-0fae4bdf0feb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff61b7d-c694-4d6d-8c65-77603db4d943",
            "compositeImage": {
                "id": "79d3530e-c49d-44de-b9a1-4e4c91220232",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d638c5b6-2bd3-4be6-a374-0fae4bdf0feb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "038b12b6-bf7d-4018-9fee-6d1536b4dd04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d638c5b6-2bd3-4be6-a374-0fae4bdf0feb",
                    "LayerId": "3e0e3cde-dcb8-4682-8f1c-b0b847b7ac03"
                }
            ]
        },
        {
            "id": "6709c06d-4fc9-46f8-85e0-5f5a509e10ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff61b7d-c694-4d6d-8c65-77603db4d943",
            "compositeImage": {
                "id": "39b41a04-76bd-4499-8fbb-fced9e2f5b0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6709c06d-4fc9-46f8-85e0-5f5a509e10ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a83cb0ab-73de-4d64-9763-1d6fae22442d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6709c06d-4fc9-46f8-85e0-5f5a509e10ec",
                    "LayerId": "3e0e3cde-dcb8-4682-8f1c-b0b847b7ac03"
                }
            ]
        },
        {
            "id": "3e3a7190-8cd0-4a2b-b0fe-89ddb880ab8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff61b7d-c694-4d6d-8c65-77603db4d943",
            "compositeImage": {
                "id": "60383365-5ac2-46c1-bd29-e7188d071f72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e3a7190-8cd0-4a2b-b0fe-89ddb880ab8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4fc22f4-7dbd-4a1a-8151-d0cd9c0fb9e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e3a7190-8cd0-4a2b-b0fe-89ddb880ab8e",
                    "LayerId": "3e0e3cde-dcb8-4682-8f1c-b0b847b7ac03"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3e0e3cde-dcb8-4682-8f1c-b0b847b7ac03",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bff61b7d-c694-4d6d-8c65-77603db4d943",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}