{
    "id": "8336a8e3-8bbf-48c8-a750-2953fd65cfde",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fntTimes",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Times New Roman",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "85d3b2e2-9b58-4037-a3df-7c1f12be39ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "0ffb1f11-d729-4bf3-8141-2354d09e3cdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 144,
                "y": 42
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "bad5cb4b-9d66-4b30-ba88-2c223eb70561",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 18,
                "offset": 2,
                "shift": 7,
                "w": 5,
                "x": 137,
                "y": 42
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "4ba9350b-be13-4b37-bfb6-ab89a8c2848b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 127,
                "y": 42
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e5d6e82e-8cd8-465d-b975-5820ca1fe0b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 118,
                "y": 42
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "b89f84fd-d8c1-4de1-bf9b-90af318aaa3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 104,
                "y": 42
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "9ecf3edd-24a7-477d-98da-03a8ee5111df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 91,
                "y": 42
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "c89fa73a-d03e-4024-aa6c-6a540aab62be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 18,
                "offset": 2,
                "shift": 3,
                "w": 2,
                "x": 87,
                "y": 42
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "9503f09a-daa8-4bc6-a829-b73eaa87a471",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 6,
                "x": 79,
                "y": 42
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "130087a4-eaa5-47dc-acc5-ac8ba760b6d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 18,
                "offset": -2,
                "shift": 5,
                "w": 6,
                "x": 71,
                "y": 42
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "cc0b61a4-574e-4040-943c-f4b7d5611a40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 18,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 150,
                "y": 42
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "20c3c42f-2977-4dfd-87ca-60b8280147ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 61,
                "y": 42
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "8c2a8d8d-a97d-41ab-9925-002e15ae90eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 48,
                "y": 42
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "54e0f46f-9598-487c-b7c0-32479189223f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 41,
                "y": 42
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "5321ac15-a9c7-4daf-b91a-c4080ccc0aaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 1,
                "x": 38,
                "y": 42
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "723c14c1-9c01-4043-b3db-8d45d958d762",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 18,
                "offset": -1,
                "shift": 4,
                "w": 7,
                "x": 29,
                "y": 42
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "e30ca5a1-ee30-4995-8d25-0d2443675f7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 42
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "7d3b8e6b-f25e-455a-a658-c7917bb6162e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 12,
                "y": 42
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "bc3bd1f0-0510-4149-b882-add23af7751d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "7e2d0434-674e-4251-8750-29d6cc8624e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 239,
                "y": 22
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "a234bae2-66f0-4be3-bb94-d54fa0c69af1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 230,
                "y": 22
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "820503fc-2c09-4ed5-aeed-4a8a0600d4fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 52,
                "y": 42
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "b0b8c906-6e6e-4783-9b46-644298504ecd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 158,
                "y": 42
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "d6ff18a4-4fce-45f8-a8ca-bf9e220ffb3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 167,
                "y": 42
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "b5a658b8-3d99-430f-a764-d000fa4c2cc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 175,
                "y": 42
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "679058bc-e237-4779-8a5f-825a58f0f1cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 168,
                "y": 62
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "f3acfd85-6b04-498f-af96-f3ad8c877e75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 163,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "e7bc6206-a337-4c69-be64-61cd08158be9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 157,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "0a718840-ee67-452c-ae91-93602865a225",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 146,
                "y": 62
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "a924aa7d-8f5b-4ef2-b369-4ee71a31e988",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 136,
                "y": 62
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "262fe420-f518-4074-8169-62dee7c4a2f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 125,
                "y": 62
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "37b36f54-fae0-464d-aad8-28ce3ace08a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 18,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 118,
                "y": 62
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "ff23bb21-83eb-4be0-a7cb-780398cc7d17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 18,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 103,
                "y": 62
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "ac56d796-55a0-48e5-a30d-b8f84100d30d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": -1,
                "shift": 10,
                "w": 10,
                "x": 91,
                "y": 62
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "6703ff11-ab5b-45dd-92a3-2818491e8f3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 80,
                "y": 62
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "d685fea3-5028-4205-972a-53e6bb6cb655",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 68,
                "y": 62
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "9c757227-fd39-4bfc-bdcf-3c5eb06463e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 55,
                "y": 62
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "d8d5c4a9-f5a2-442e-8c1c-087eb8ea7c4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 43,
                "y": 62
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "fb62f844-dc9a-4216-a777-55463fc5e52e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 30,
                "y": 62
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "a8c4c279-aa1f-4ad7-afcc-6aec0313354d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 17,
                "y": 62
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "3bc46209-9a01-43e0-a86a-81bd7766c677",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d1d2c13a-c9ef-4742-832c-1febe976f1e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 7,
                "x": 239,
                "y": 42
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "2be6c2cc-ec23-4809-9998-3167452dbc43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 18,
                "offset": -1,
                "shift": 7,
                "w": 10,
                "x": 227,
                "y": 42
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "b01c8968-b7ea-4c4a-a1d4-25fc62f152f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 213,
                "y": 42
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "b42b5fcf-c052-4056-955c-eb5f8b35b4b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": -1,
                "shift": 9,
                "w": 9,
                "x": 202,
                "y": 42
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "9a82b199-84ea-499c-8ce4-e5214658d5fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": -1,
                "shift": 13,
                "w": 16,
                "x": 184,
                "y": 42
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "cadef11c-b779-49f9-9f58-0cbed7331c63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 216,
                "y": 22
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "7cddde20-eadf-4278-9049-d076e13a39b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 204,
                "y": 22
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "313bfa57-c175-4c54-9fea-d0151ea0d05b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 192,
                "y": 22
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "3ef6655a-d77b-4f2a-985f-45be8dc7da26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "bee3356c-5896-4451-b919-30638e7a2faf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "de2f866e-0901-4a70-8f14-b7ca4362467e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "fcd666a4-776f-437a-aaec-70c178a3f4bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "b3c37c4e-a20e-4cbd-b3ec-0cca56d88be6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 2,
                "shift": 12,
                "w": 11,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "39fbde64-b4a6-4772-bbef-8a17dc825654",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 10,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "d333c67b-0b0e-4a4a-b234-2f34e667c0cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": 2,
                "shift": 13,
                "w": 13,
                "x": 145,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "90b12d5b-5314-45da-9e6a-4faa1c45e03f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": -1,
                "shift": 10,
                "w": 13,
                "x": 130,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "0dff51b7-ce82-450a-8550-0c695850f3d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 10,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "8205071e-3b22-459b-8e12-b8e41071faaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "b88864e3-9150-4421-b2a0-dabdafa6b0be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 8,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "e7238fe1-9695-484b-914a-bd6a91642963",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 18,
                "offset": 2,
                "shift": 4,
                "w": 2,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "94bcc310-6f8b-4488-8a18-614fae585dec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 18,
                "offset": -2,
                "shift": 6,
                "w": 8,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f7f946a6-20c1-463b-85d4-3c8687bf0fcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 18,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "30799288-f2b5-4bb5-8c9b-65efd343f047",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "6ddc8010-27da-4ae7-a636-1e52c85ae7a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 18,
                "offset": 3,
                "shift": 5,
                "w": 2,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d5ebd859-8a5a-46a1-93d1-4c20093246d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "e9e11fb2-b0b5-47b0-81e8-e3b0e603ad7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "cc62bf3d-e965-48bb-a861-511ecb9a66cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "fe02ef40-149a-4907-a9f7-8690fb852d9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "4380e994-f623-498b-bd2e-583e5524a404",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 18,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "9ad1c675-0877-4de3-b1ca-f5381ec97196",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": -3,
                "shift": 4,
                "w": 11,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "7c49bd62-41f9-4541-bee1-1b960b773779",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "96db9337-ee81-4c3c-8248-bff9f255ffe0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 83,
                "y": 22
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "ea622025-23ba-4114-aa7b-bb393dfa9770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 250,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "ad4103a5-8098-4bef-87f8-7d0e719cb465",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 18,
                "offset": -3,
                "shift": 4,
                "w": 7,
                "x": 175,
                "y": 22
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "5e5db159-4292-46c7-8e0c-1f398bd24b7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 18,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 165,
                "y": 22
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "71f869cf-4332-4228-9840-91835b31b65a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 159,
                "y": 22
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "46f37797-1cf6-4806-8402-c33789e3e93c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 146,
                "y": 22
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "04ee5a8a-ee35-4a3d-b300-e5b289e8683e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 137,
                "y": 22
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "118171ef-eab9-4131-8545-170c71c15d5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 128,
                "y": 22
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "9c470906-944a-4fea-b128-2154fae14387",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": -2,
                "shift": 8,
                "w": 10,
                "x": 116,
                "y": 22
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "827d7796-85d4-4bcc-89f5-84fac7ff1d4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 106,
                "y": 22
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "19a11dbf-501f-499a-b565-3ba626b83773",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 98,
                "y": 22
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "caa06fe2-7c3f-4aca-8627-ef739671a567",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 184,
                "y": 22
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "26339a28-fcea-4978-9215-de0a9c9154fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 92,
                "y": 22
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "408e2a2c-8bc7-4c7b-9184-e78a27120a33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 73,
                "y": 22
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "81659090-2b02-4750-b62b-c73950e60ad5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 18,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 64,
                "y": 22
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "8d7fd9ad-7e44-4ef1-9f34-5f230287a42c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 51,
                "y": 22
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "11cbadac-a918-4fe2-b060-39db4d6310ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 41,
                "y": 22
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "1ab08c97-2746-494f-bddc-0a5545a67f60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 31,
                "y": 22
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "96f86895-bd9e-4652-94dd-1b0f4d8d5071",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 23,
                "y": 22
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "1fa09fe9-3155-4b2b-9f38-2449c90e7d13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 18,
                "offset": 1,
                "shift": 6,
                "w": 7,
                "x": 14,
                "y": 22
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "b5ef1399-7af4-4088-91b3-8919c83ddbb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 10,
                "y": 22
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "bc09dd40-9f18-48cd-ba04-7eea597b2e42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 18,
                "offset": -1,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "dba76ccd-b9ab-422a-9c83-8006bd800fb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 178,
                "y": 62
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "b27f9b93-0d99-496f-af6b-06a6ae06e2a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 18,
                "offset": 3,
                "shift": 16,
                "w": 9,
                "x": 189,
                "y": 62
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": true,
    "kerningPairs": [
        {
            "id": "13116d35-b53d-4b61-98a2-7bb49612a360",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "0b513f80-d078-4b18-92e8-3e6c327af616",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "ec617325-ac71-4f5e-89ac-1119e94f5787",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "2f5eadf3-957f-4c48-8848-ecc09ad525b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "ebe64208-4174-4a0f-8a51-804586f18d78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "b0248d1a-360d-4734-9967-f9856beaefe5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "53aa3d1f-7a00-4a22-8944-b6a55843f5d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "ccc2acab-225d-456b-8d5b-332a841bbd6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "88ee988e-579b-4efd-85bf-40448bb73dbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "79c222ae-cf18-45fd-a0cc-09071706c52f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "4f29f39f-1670-4e4f-955a-b2db43c97bc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "70f8dc8c-5226-4629-8620-277dcccb8434",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "6c36568e-01aa-4cea-8fb5-27f0c15c6853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "a69c40e3-ea3e-4dcb-ba32-a3ed46fa52d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "9986efb5-015e-42e7-a448-901678284d85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "625d5c10-19e5-4aec-a59f-d28f8b11016f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "169c765a-2758-4679-afdf-d152248bf64a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "1ab6ed9c-0c09-404d-a714-6709204d6e56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "4a460336-a1dc-4d33-8f85-e5d6978baa38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "6fc55822-8f2e-4304-b2ee-8891f64f4e25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "e29bc630-acee-44da-bf35-21196b4c5840",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "ecf77a35-b838-4d36-a2e3-34a22dba543e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "93b6f812-61fb-49e7-9e5d-724076ca38c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "4f1b6a11-d73b-425c-b6ca-32882ea61ccd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "5b1e8b96-56b6-444c-bf4f-c810d1db5625",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "773790c7-8130-4c2e-b16c-0a5db39c1cfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "15a6f224-d15e-49f2-ab52-e351fbbedce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "539818c3-5d6f-422a-a620-d034337a2f90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "f3fdbb85-d95b-4963-855e-7b86bbec50d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "60881a77-e691-4d21-a906-386c5efcb457",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "18c05eef-9619-4e1b-ba5c-1d83d46b173b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "a8e1ac8e-47af-43c0-be88-f030b5934af0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "6340cc25-1de2-439b-9c9a-ad75da119604",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "4415e27c-dded-4442-b765-c02f36dbdac0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "60b95570-d6cb-4bb1-92a8-e502c78f5898",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "1d38837f-9b84-4105-8104-150bdc45c2fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "e5f39bad-97de-4f20-8969-c13f87a56265",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "4747e77a-e621-46f3-8b18-143f00c32ecd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "84d1ac0e-03d8-4f0c-a1cc-a9e0b24ad612",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "7f56bb95-8e54-4d5e-bca7-e24a09ee7b04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "3399486d-fda7-44e6-983d-e7e67ee442eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "5ecfd805-bb0b-41b9-b326-54f467bde711",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "d8b75625-bb34-461d-8112-39f2e2371dde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "9040b088-acbd-48fa-83b1-de1907bd6169",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "bef9dc4e-d394-49f6-a971-a7f757772fbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "e476551b-d605-4215-b84f-180ac79995b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "35564d1c-0a25-49e7-a6ad-670d5c80fb62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "7617557f-1b40-4be8-affc-c20e79a492fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "320659ac-dab9-4d83-b1f6-387b623b32c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "89b7d099-e84f-4da0-b4d0-f307128c3e39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "7a072f09-08d6-4fe9-88dd-58482a76c0c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "3380f610-6ec2-46a0-9c77-c6d36ceeed61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "4ccc90bc-fe43-49db-a23b-55e84133832d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 105
        },
        {
            "id": "41dbe650-e9f8-4696-8583-6af343f1f6c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "74505534-fae6-48a8-85d3-001dab348b97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "18649124-4a4f-4cf1-8bb8-0a02a0c9b647",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "9feb7bd1-3c5d-4c1d-b6e1-a5160121d483",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 121
        },
        {
            "id": "815422dc-064b-4cd6-a577-da43765ddaa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "e251584b-831d-40b2-aef8-d7d2e058b101",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "37d21c67-21f4-451d-8787-857132a607a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "b444d378-3fec-4dd6-895c-cd7673a59a88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "b0e4e397-df8a-43a5-90d2-783420035a04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "4c61d855-aeba-49fd-9d18-8f09d6c4db82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "60795c6d-3ae7-471c-a9b0-0a8d967d9956",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "c61f9b3a-2796-4fb0-85a5-4df90b1c5ba6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "abb8b0d4-7458-4a19-8dbf-e4b8fc60b3b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "50952db2-659e-42ae-9e97-947b0fb494f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "413fac65-5cd7-428b-8d73-abd61426b9d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "ff7834a3-35e7-40be-94be-61294615f620",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "2e68b098-298a-4df5-9564-cbc63f974e5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "3598e596-b098-455e-9d57-d166363a3718",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "f61c2fc7-c926-4bd2-842a-57c8a94cb3c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "af4af78e-3ff8-432e-8083-537b68b63123",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "7f513d65-acbf-4e3b-bb9c-af2a7456cd99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "714315cf-abaf-4483-aaa5-1b975cbc01ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "4c9d579e-fa33-4957-8b57-6759631be9b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "e6069299-8b48-4f91-b1b2-1f8abe9236c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "2458d755-f6b0-4304-aa8a-67e5d3609b7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "e90b0611-3adf-42fd-84a8-1e3c25156547",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "e0bcc3f5-faa8-4848-8d7e-8de0424e68e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "e50de84b-f76b-435e-9d1b-59a7373af39d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "88504cc6-45e1-4d4f-896d-b9fb8277f518",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "9176997f-28c4-4816-9c05-77f74bc87c17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Italic",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}