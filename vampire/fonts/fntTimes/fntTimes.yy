{
    "id": "8336a8e3-8bbf-48c8-a750-2953fd65cfde",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fntTimes",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Times New Roman",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "cfd00845-c5e1-43fd-a418-89628954df1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "363a00d9-3667-428e-ae29-f5573b8b359c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 102,
                "y": 40
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "4c530424-3c35-4e76-913b-627468b534fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 17,
                "offset": 2,
                "shift": 6,
                "w": 5,
                "x": 95,
                "y": 40
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "074cd554-e9b8-4240-b755-568eb500c7b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 86,
                "y": 40
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "9a8bc31e-2582-4e8d-8f4b-14ce3d5059a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 78,
                "y": 40
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "dbf483ac-d14f-431c-b854-4253196525d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 65,
                "y": 40
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "edb59bbc-5cc7-418f-99b5-cc74bfd6b7d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 53,
                "y": 40
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "7a75e1e0-cc43-425d-974d-5bff3062af95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 17,
                "offset": 2,
                "shift": 3,
                "w": 2,
                "x": 49,
                "y": 40
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "d26213f2-fb9f-42be-80ae-e2aba50e624d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 6,
                "x": 41,
                "y": 40
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "4d6d091c-603d-46af-9d85-22e8ff1a5958",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 17,
                "offset": -2,
                "shift": 5,
                "w": 6,
                "x": 33,
                "y": 40
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "7ef92831-bfc3-457d-9f02-2c114a18ffac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 17,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 108,
                "y": 40
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "fb89ce44-1816-4655-ac43-68892e6c8944",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 23,
                "y": 40
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "47e6a622-1247-47a4-b747-b3c4899f8c4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 11,
                "y": 40
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "bd6c1c29-99e4-467e-a548-3ab671dd54e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 5,
                "y": 40
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "94195e0a-49a9-4c71-8772-cd91ec01543b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 1,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "cc5ef3dd-8816-4d07-8f22-5ce16b9ce16d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 6,
                "x": 247,
                "y": 21
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "3f7f4735-9667-48b2-bc07-44dc6b83b93c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 239,
                "y": 21
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "82845668-9a59-4b14-b044-ef7c371ce3ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 232,
                "y": 21
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "83c758ee-b52f-46be-ba08-7aaf7b3f93e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 223,
                "y": 21
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "4fff8ac6-557c-4306-954e-a37c2b4728fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 215,
                "y": 21
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "03ab6c37-bca9-4dd0-ac4e-f464429728db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 206,
                "y": 21
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "c6153755-498c-4e7b-83c0-b0bce02660f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 15,
                "y": 40
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "08c67a89-04c9-4eaa-93d8-8d453407b0bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 116,
                "y": 40
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e227175a-731d-4137-9eff-552b5fb440b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 17,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 125,
                "y": 40
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "242593e4-0e0e-440b-906d-c2cef1e751a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 133,
                "y": 40
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "0805e239-f801-461c-9fda-02f068eaf31d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 112,
                "y": 59
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "5925a134-1f19-4524-bf48-48a3c859181e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 107,
                "y": 59
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "1f48acdf-6b63-443d-9ece-032aad54506e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 101,
                "y": 59
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a2c2cd6c-e065-4cb5-a5fe-a0e9cb99a3cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 91,
                "y": 59
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "bd570a01-05f4-4a4b-9512-5e74b628b513",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 81,
                "y": 59
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "6b94f469-57bf-40b9-a02a-016d3cecd90b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 71,
                "y": 59
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "bf0f6a1c-a6e6-4e1f-b496-6292e9334767",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 63,
                "y": 59
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "fc24e886-a8b3-4720-9869-b4ceadf80d19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 17,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 49,
                "y": 59
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "ec25f446-d130-4936-822c-bc6488bcf488",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 17,
                "offset": -1,
                "shift": 9,
                "w": 9,
                "x": 38,
                "y": 59
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "a0fdbe2a-0b6f-4b19-b246-80fe081a6235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 27,
                "y": 59
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "77617273-9c74-49e0-830b-bd87e1a4d178",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 15,
                "y": 59
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "fcd12e73-b76d-4288-a295-dc6735b56c30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 59
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "8f888012-1ec8-4ca0-9012-4f9cc066fe92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 237,
                "y": 40
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "1c0c5250-9e6f-40a5-beed-ab37f756b06a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 225,
                "y": 40
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "061e05b6-ee4f-4202-9757-83582c40dbfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 213,
                "y": 40
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "58a8a2b5-5285-4901-9bee-e33378864f23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 199,
                "y": 40
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "664191b3-bfa5-449b-9c13-8dd928fb0a94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 191,
                "y": 40
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "2630aec3-af5a-4c1f-a4b6-5093fa2e97cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 17,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 180,
                "y": 40
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "9a01fc9c-9325-425b-bc0b-d07641c855e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 167,
                "y": 40
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "56ad3159-26bd-4c39-9220-943712be3793",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 157,
                "y": 40
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "6a00a7f0-e230-40ee-8230-12a36486bcba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 141,
                "y": 40
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "e961b1ab-9a24-40ac-a396-cd54f4393ecc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 193,
                "y": 21
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "7de9a8ef-3e6d-4419-b759-a0e55b193e38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 181,
                "y": 21
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b02b7528-8861-4868-b7c1-2ab7fdd8f017",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 170,
                "y": 21
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "9931d485-ff92-4547-894e-68b6b9e66e44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "bfef0cc9-31fa-4878-9e2a-63814370f641",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "81547c6a-8779-4a71-82bc-4e45b2bd9171",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "dfff19eb-6799-4026-a2d5-8b4e0dff4afb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 9,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "e6eb702e-df19-4de5-80f7-e963b67b3953",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 17,
                "offset": 2,
                "shift": 11,
                "w": 10,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "f1d89e24-febe-40fd-9564-f6d2ee11b2ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 17,
                "offset": 2,
                "shift": 9,
                "w": 9,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b96b0bcb-64c6-45a1-9fc7-80fec5b886e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 17,
                "offset": 2,
                "shift": 13,
                "w": 12,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "14077265-09a8-48b9-8d45-64ccd8234417",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 17,
                "offset": -1,
                "shift": 9,
                "w": 12,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "56408c95-2f1b-4da0-b7ae-47682ff4ecc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 9,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "91a98bf4-5f4e-4760-b9c1-e32e92efda65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "7ca2872b-7d71-4d0c-a17b-6562454177af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "8f67797d-0ad5-4a53-94d0-feca46c162cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 17,
                "offset": 2,
                "shift": 4,
                "w": 1,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "47bd8013-cbfd-4c50-a847-7c577ac53ed2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 17,
                "offset": -1,
                "shift": 6,
                "w": 6,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "e59c5a00-4776-45bf-9429-0668bbb966c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "c0e66e26-ebac-49b1-8217-7b72676b53f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "33c8d8a9-8e59-43c1-af4f-a0701710180e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 17,
                "offset": 3,
                "shift": 5,
                "w": 2,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "4e5d56fd-84ce-4725-b98d-e872c8347966",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "f8698ed9-0d46-4d40-b3a0-e71ba9734dab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "f65887cb-9ee0-4612-af3b-5314717db3df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "54c1a41c-fa04-421e-acd5-dfe09db8199b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "cf01bfde-e5c8-4d12-be5f-b795c58aad16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "34f5b3da-ee6a-4c09-98c5-5bf0d6d71905",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 17,
                "offset": -3,
                "shift": 4,
                "w": 10,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "9c4e91c9-4aa7-4e68-977f-9c44d9828ded",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "c29463d3-af38-4167-a05f-ee0bf150852f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 68,
                "y": 21
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "d4bb076b-bac9-484f-91a4-847ae415ee50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "ea0d03c7-f31b-474b-9915-5f2b6d067595",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 17,
                "offset": -3,
                "shift": 4,
                "w": 7,
                "x": 154,
                "y": 21
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "545a2daf-13ae-40fb-88ee-71ee15749b17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 145,
                "y": 21
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "1f37ea5c-c4a2-449a-8d2c-1ffeed7ddf5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 140,
                "y": 21
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "30adff56-1e53-440d-8cbf-835dd4656079",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 128,
                "y": 21
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "7d02dce7-6ef1-4011-9112-b758757c930d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 119,
                "y": 21
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "6898295a-febc-430b-ae3f-47e8e511d535",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 110,
                "y": 21
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "ae7f6de0-b6ca-4151-930c-ca4585317a89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 17,
                "offset": -2,
                "shift": 8,
                "w": 9,
                "x": 99,
                "y": 21
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "b63c45b0-52b2-4ab1-95b3-2f3b630131b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 90,
                "y": 21
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "3c3050cd-e8e3-4b23-8731-9d31dced79a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 82,
                "y": 21
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "4121e092-463e-4bd6-97c0-67f9b9548ff5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 163,
                "y": 21
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "71352204-5ba7-4830-9782-9ec287f4ffa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 77,
                "y": 21
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b68653c4-7373-4da3-895e-19697aca62d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 59,
                "y": 21
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "b64219f8-4f90-47f6-904a-cd0790f5f9d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 50,
                "y": 21
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "0850db01-b62a-45ce-aa7b-22b523b09110",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 38,
                "y": 21
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "adf89b2d-8c7e-41fb-8329-f2a40f5dc765",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 17,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 28,
                "y": 21
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "ed528585-9210-4f59-b4f2-8628939ed20a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 17,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 18,
                "y": 21
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "02d24352-77f0-4863-970a-822ecd63962d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 21
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "ee85483a-1c8a-4a7e-9af2-a18b824a21b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 17,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 21
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "44d49fee-bced-4a23-a64d-d6967ab79df2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 247,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "540330f4-0e76-4f7b-88b6-5382b3376339",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 17,
                "offset": -1,
                "shift": 6,
                "w": 6,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c1f277cb-e9dd-4b33-b715-4648560f46f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 121,
                "y": 59
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "979cb7a3-45c9-4481-ab30-5648b3cb1b9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 17,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 131,
                "y": 59
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": true,
    "kerningPairs": [
        {
            "id": "f5c0b2b9-7f7d-41cb-8c4f-503b32fbf256",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "bbb92e06-e542-49e4-a643-fbb4eb56d03e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "739a4faf-c709-44bc-bd8c-825575769494",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "f255629c-f4c9-4413-8cf9-2513c47b5f9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "3ed0c93a-310d-457c-9dc3-0c1dae5284c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "f0c3454f-cc20-4e70-96a5-a492555978d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "1ba23270-e236-47d7-85e3-a6869cc1baa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "64b0d1ef-d009-4028-b385-1e931f00aee5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "4db07a54-aadb-4bf3-9eef-87f0b604cfc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "7f27bea4-6655-4236-9e4d-575d120dd322",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "125c1777-e7e4-4030-8373-d8ad655bd54b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "2a0d41e0-f26d-4535-bf93-9f0a802c1bc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "7014d6b5-7dd9-40f2-a95c-013aaeac4f1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "a28241a4-de6e-4e4d-a283-5582377c36dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "97a2219a-89c2-47c0-bf72-796a35509c7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "b97fb76c-b112-4185-b4ed-63c5dbacdedc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "fb5740a7-e523-4091-8410-531f633e3c27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "3341b4fb-e0bd-4935-83ad-b5e37f5aa86e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "397e1b57-f7f5-4bdb-a79b-3adec9b530b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "c4f227d5-485e-4037-8d6c-a4138d6bb605",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "d645ee46-0830-4134-9edb-fca4c2a98169",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "a5377231-be4c-4811-906a-8eb63e8374cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "379487ca-7f15-431b-9de9-44dc61ac1bc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "b6eb83a2-910c-49bd-909f-aa41cb5c3a2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "ab586cf0-33ca-483a-8528-e715179b514a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "4e15e609-b0c0-4fbc-abdb-8c2410bb25aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "019e2153-4f1e-4d83-9443-fc3767e4e940",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "2cfbda2c-9d23-4142-bba0-abbe0d7b9f97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "7ac351d3-1bfd-43d8-bb0d-545cebb21d6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "12248b1b-3f2a-4291-9d37-13775ee9063f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "7fc3cf6a-b23b-44fd-9fb0-3cd11a547dad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "5604d8c2-5c40-4c57-8bca-a1b715736e12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "719d930d-ac6f-4036-b242-a32bbfde6ce3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "a0fd6d0a-5e94-449f-806d-7e9d37be13ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "33f7e335-eb4e-4eaa-a2c9-c069339c7efb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "eac9100f-4d94-4b5d-b10e-2afd83c1f0af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "2e97aac6-8cec-497f-8b54-9157707b1511",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "9e257242-02f7-41ec-87d8-79e5e110f0e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "c8e0249e-b811-4eba-9400-28f54cf25c45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "136291a8-fb87-4d3b-b79d-cf3c9e046c67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "56d8feda-7b7c-43a6-814f-aded6af9b245",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "8a721d9b-5742-42ca-8270-697d26f0ecc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "ae442771-5b2b-4906-968a-62bb6ced2575",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 121
        },
        {
            "id": "c75a0cfe-6249-464b-886e-454f2ddcebe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "67de88d1-35a0-455b-86e2-c6b99e70b460",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "d16bff47-804a-41a3-ba23-ef6bb7e560cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "60961526-7518-459c-94c5-959c15214e35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "2c52fda4-9b25-49dc-892d-9df202ff28ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "2f81beac-c2fe-43d8-9077-aee89144fcdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "6ea16367-8627-439d-a88e-ae375b800946",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "5366e50a-6dce-4b6d-8789-e2892ea78963",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "7ae540f1-65da-481a-83a4-9a1fdc81776d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "aacf8667-cc45-43fd-9529-65edb0fc0e6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "44f2ea35-4a85-4ac8-b782-3c1ef6f0bb10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "70e68118-9be3-4b68-9d3a-3c8c576c151d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "27052146-6dc6-40ba-8bc5-d2159bd2c0a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "d18e04f0-fdf1-4722-87ee-09d7f4db9b50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "d0fbc905-eb4e-4e93-9e42-0f0c30719c83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "a9f2c391-bd64-4d57-baae-13524f8f78fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "2b7f9060-af74-4ca6-b69c-79bbd573fdef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "f348cdf6-dc43-4a04-a16b-b42e5be220a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "ee078c02-9bd5-4686-95d6-48ceb72aeda6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "aeb69546-38c1-4fbd-bcf9-9a6ced979727",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "cfa70daf-d6ef-48a3-82b3-0588d353e1ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "0dec5342-11fc-460d-a2ed-4407ef043c85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "d691c143-36d4-4db1-ae5b-0a7370703b73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "c956b462-25e0-4576-bc57-dbce383be6fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 11,
    "styleName": "Italic",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}