{
    "id": "8336a8e3-8bbf-48c8-a750-2953fd65cfde",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font0",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Times New Roman",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "483b7ad1-b80f-4047-861e-90b1a7ca2015",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 15,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e0816011-f49e-49d0-8798-e455e1049fef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 72,
                "y": 70
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "560f3b20-07c5-41b2-a8f1-60017e42c858",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 65,
                "y": 70
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "36d4abc0-f4c1-48ca-a528-0565064263a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 57,
                "y": 70
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "6292e350-71f8-463b-9113-7aadd64faa55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 50,
                "y": 70
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "b2010fcb-2f24-4333-988c-4218e00ca67f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 15,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 37,
                "y": 70
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "8d8d6a59-25c6-4fe1-bc98-dbbbc504de7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 26,
                "y": 70
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "ea46214d-e3d9-4870-a92e-2b5036978612",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 22,
                "y": 70
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "ebb96d0e-8400-4bef-b316-47ad67b5742c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 17,
                "y": 70
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "b91d9386-fdc4-4b4b-97ac-fbe6e5d70489",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 11,
                "y": 70
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "73d58a3b-216a-49fe-8c17-4958ee13876a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 76,
                "y": 70
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "12952bc0-732f-4742-b55f-7f97c2aca58f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 70
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "b9e0c470-05c5-47e6-bd7d-2b47a9b5e6cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 15,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 109,
                "y": 53
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "c5962838-f84e-4af0-ad49-6c35191633a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 103,
                "y": 53
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "45299ddf-909d-43e0-ad32-c334bbebceff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 15,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 99,
                "y": 53
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "0cd6548b-b70c-403f-9122-6e3eb4a56baa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 93,
                "y": 53
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "1775ca30-b4f3-4f4b-a30b-1420b49ecb15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 85,
                "y": 53
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "fc14e5be-3c16-4c9b-8e51-6fbeb65c5d77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 78,
                "y": 53
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "18a6da2d-9ebc-4920-bc78-0e0e1cb313db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 70,
                "y": 53
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "f4f41840-f5bf-4b77-b5d3-d9b4140b19cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 62,
                "y": 53
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "3abb2873-730f-43c4-8fea-aedeed05237a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 54,
                "y": 53
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "e133b00b-af95-4dc1-bcb1-5829484c8cd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 114,
                "y": 53
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "976ab997-435d-40d5-8f1d-ba35a463633a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 83,
                "y": 70
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "019298be-4b26-4f63-b969-8fa0618272f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 91,
                "y": 70
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "66a40cc1-e395-4e7d-b404-34a8363b31be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 99,
                "y": 70
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "c8b49123-2cd6-425f-bb0c-ef4011944f30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 57,
                "y": 104
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "4c435cf8-d597-4e88-bee9-62de41907162",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 53,
                "y": 104
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "4d9808b1-1c68-4746-869c-bab17ae1fec0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 49,
                "y": 104
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "f7b38dfe-94f4-4841-a31f-807b935b730a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 40,
                "y": 104
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "e0eac58b-7957-46e6-a6cf-8eb02ecd47ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 31,
                "y": 104
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "576b1248-9c9b-4f5b-b956-3121dd971691",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 22,
                "y": 104
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "9b55994d-95a6-4a49-aeee-2b0959a4be43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 15,
                "y": 104
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "01a70434-dd4b-4ce6-94c6-1499c58642f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 15,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 104
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "760aafae-62b2-4a55-bd2d-2a1efd047285",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 113,
                "y": 87
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "d258a524-7d96-4b36-b9a1-4a99bc54b35d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 103,
                "y": 87
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "fcb66eb4-5ec8-46bd-9cde-ca441dfbe6de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 93,
                "y": 87
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "c05edab7-6dc8-4709-9c20-b6909f60f9ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 82,
                "y": 87
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "09827932-f5f2-4420-b190-1e8a1e054adf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 72,
                "y": 87
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "a1217816-2411-4991-922f-51ecf5450a19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 62,
                "y": 87
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "1b0171cd-7f33-4e79-9d82-6a7f3311bff2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 51,
                "y": 87
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "cb8eba1e-d513-4874-be4b-dc53b035c64a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 39,
                "y": 87
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "8b509913-4f26-4f2f-96df-ccb62ef65c8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 32,
                "y": 87
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "bc58495c-2550-4c85-9874-24688e1a29ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 24,
                "y": 87
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "9cbf1ac9-19b6-4b82-8a02-6cdfd12b3c9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 12,
                "y": 87
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "17aa7e49-2601-4bd9-b958-779789b2a934",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 87
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "59663472-3e31-43e2-9879-cea246bb51f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 107,
                "y": 70
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "4621d817-2ac0-4c8e-bf36-8639ac061849",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 43,
                "y": 53
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "c7e81885-3e87-4b59-b9de-72c15c042cc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 32,
                "y": 53
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "5fffd31e-d221-4bb6-aa7a-de28783d68c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 22,
                "y": 53
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "6c96cc26-299c-48a7-a0a1-4eefccd3207e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 74,
                "y": 19
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "fa7cd448-68e8-4d93-88e1-5d898c4b1663",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 57,
                "y": 19
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "e0c398aa-be90-4f2a-a123-7846f4efc688",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 49,
                "y": 19
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "43104ed5-2c60-4357-ae96-fe5967d64455",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 39,
                "y": 19
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "f5c07b5a-3c73-4462-ba26-6bf95164e238",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 28,
                "y": 19
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "46a7cf9e-f0df-440e-b261-3ad6496f386f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 17,
                "y": 19
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b788a583-d897-405a-86a0-22a5f4df8e27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 15,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 19
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "149d5027-8af4-41c6-98aa-83240d4f1b85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "5c154258-8ca0-403b-8e67-354d4de937bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "0d90d935-2fa1-49fb-abe3-80f6479f727b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "618cde56-676a-4076-9e8d-dbf2a6254060",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 69,
                "y": 19
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "86eff495-1375-48c6-af1c-964009d1f003",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "1b098013-d4d1-4b1a-9d6f-3128c36b9838",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "0bee3f85-719a-4f27-956c-c9e063ebea5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ed5dd2c4-354b-4c35-ad6a-6c131573e487",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "32f2b21e-be16-4a3e-9c47-f8346694f241",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "3fbc89ba-3d77-4d52-b027-a38e3e923305",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "c1a99713-96cc-42a9-a876-d44ec832826f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ff9ebf62-adf3-47bf-8369-fe66801b2d8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 15,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "51b68815-22ec-46a4-ab42-9c869ef3cb40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "b0754ec4-d9eb-4ba3-a7b3-ba5ed35cb42a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 15,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 7,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ce58063e-9230-4474-8f73-286707f38770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "a17a2eb1-8a2b-4d3c-9478-b27e4e13723c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 85,
                "y": 19
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "597f63be-85bf-4575-8790-bcc1d0a75ee4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 46,
                "y": 36
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "639c2867-165f-42d9-99f0-ef8f8d0c1b2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 93,
                "y": 19
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "a0616f34-5346-4ceb-8567-f71896264b9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 11,
                "y": 53
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "0403382b-469c-4a72-8743-75f0aa99498e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 53
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "75f8b2e7-9a76-4ea5-a642-f223b6915158",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 116,
                "y": 36
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "193f0a44-ecb1-4307-8cb1-6decea2aa739",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 15,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 103,
                "y": 36
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "6d592901-04e0-48a0-b18d-88f1b852a3ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 94,
                "y": 36
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "3d1fd2b5-b071-4956-a128-307487a8377f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 86,
                "y": 36
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "9983f36a-b600-408f-9bca-2f27799a5dba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 77,
                "y": 36
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "0b6e66cb-2163-48cd-bd9e-83d8601fc1c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 69,
                "y": 36
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "ab010e16-e4c6-4688-9074-4a2108e7bc4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 61,
                "y": 36
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "30a2d2fc-3704-4344-ac6b-e46e4ad4171e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 15,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 16,
                "y": 53
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c5e81f29-5482-4a40-8c8d-ae80462e192a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 55,
                "y": 36
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "4ed523a1-9c30-4a09-9e47-52fca78400e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 37,
                "y": 36
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "419f8af4-4250-4cc3-afbf-3136e4e34853",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 29,
                "y": 36
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "e30e8507-0aef-4703-bccc-3c61d87911ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 18,
                "y": 36
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "eb39d5ef-e7f1-427e-9190-5adbdb67b8ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 10,
                "y": 36
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "2e48c1b0-523e-4cd7-8630-5ba90888b0fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "c674ab29-565f-4a9e-b3d4-d6206847f49d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 112,
                "y": 19
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "cc1449aa-0357-46af-a525-114bf59a5d8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 15,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 106,
                "y": 19
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "16d2b155-b72a-41a5-b87d-50d7b3bd5702",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 15,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 103,
                "y": 19
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "6f7ffb57-1746-43f2-820c-485ef08a9ddb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 15,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 98,
                "y": 19
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "252121f3-a186-44a7-b6a3-0dc52676d3e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 65,
                "y": 104
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "15b2dfcc-c3b6-4d55-8ac7-7cd7a1140e49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 15,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 74,
                "y": 104
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "a8abe583-f67b-479f-a977-b6164c8f36ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "fc98893d-bd8f-4ed6-9cb4-64abd4273a6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "6f63c518-d9bd-4f73-9652-e39eeb674990",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "df42e759-5afa-4313-96a3-0c76a9a3eb10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "17a29723-37c9-453e-b401-e8b6e1b320d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "19f9521d-f7b2-478f-8d31-e61c99b3e354",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "503d8386-c333-4585-b1b6-cd89c1b51649",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "e3487ed0-405f-4c92-b0e4-2e21b09a51c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "51a0d8c7-9818-4eb2-bf4d-e7ce94f22e31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "b7c849a4-7ed2-4981-b163-f502293d904f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "5a869eae-42e4-4458-b9a9-0311d8b5da61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "7ef79b52-4ae3-4f0a-b394-26f0e3dcf0a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "bc2098bc-1817-4d22-b3e4-742df310b7f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "ab4105b2-2cbb-4ff4-9a52-edc93161c643",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "d14593b4-62cc-40a9-aff3-a41c458f303b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "8c60449d-0e5b-4e64-9746-1f11128c6110",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "e0376332-fd28-45d3-95fd-0a019cc1ac1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "6ac72eca-fa94-45fb-a250-09537e3bfd10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "c884ccf3-c588-41c6-adde-70c97791c07f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "377e8bae-66e0-4052-98d3-2c29b69c89ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "da73bb08-68d9-4da1-b117-1147610c0360",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "7e810d64-d511-46cd-ad95-83e5be15d061",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "bdd3f3dd-097a-4732-ae1f-4d07fb227bdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "09d4a598-434d-45f9-98ac-48f2e00b903e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "7a0555d1-136c-41c6-8806-40e5e0aa8b06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "07972afe-7baa-426a-aac9-335dfa7ecba6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "239ab3de-03bd-4ebd-8ed2-74dd4deda6ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "9b01abe1-817b-45a1-b845-2b0649208661",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "75a8eb93-925c-436b-aec9-78b7f89b1401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "58e4c8c0-8eba-4bb8-a812-d37ba38acc39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "c065e669-439f-4201-9a1d-404a4a1d530d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "0c0a7add-b929-4d97-9128-f70e0920b505",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "dd961b99-ff83-4720-8ade-bd268259cf6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "1640b5bf-aa0e-4ea8-848f-ad11495827f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "ba14def2-6d48-4b31-8c96-6e67a44d7bcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "57ce8e15-37c1-485b-8c80-972ee3df5d99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "da9740cb-457c-4304-837f-90b6b629bbe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "25f75992-c055-4452-8d7a-4348c7fc810d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "684fe6a1-d750-4c54-989f-0acc02d80f42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "9781183f-ded2-44d5-b51c-1dc87feb28e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "d867bf21-e2d2-4d4a-b13b-6f5091bc970e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "efa30fdb-c4bc-4937-825d-750281391ed3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "41754716-3d14-4c26-8f54-30f58745f5fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "6a9e5b3b-45cb-4bed-aeb6-216fd8e71e86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "9fb43466-3422-49e9-9a87-672950eb9487",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "404ce8b3-db2a-4500-9a77-c2939aac7921",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "30e30fa8-cdef-4b3a-b709-8fb8c1b7b161",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "cc023fd4-6f87-4834-a9ba-6f001c2edaf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "0e578831-c00c-4706-91fa-ad29a533287c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "8c7c5c82-a4ac-4d46-ae36-061bdccabe0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "af9b584a-707a-4472-951b-1cca2d7439de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "85bfd1f3-af1f-4420-bc25-00054198b9a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "77dcbf24-2280-4f7c-8655-4c51691a6dee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "8da2dcb8-4c49-4ed3-8f71-0745057f6172",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "873d4bf6-1ba6-4367-bebd-680410c0b7a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "391304f6-669b-448f-913b-ce78373b5a0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "1a9c336c-5ea7-42b7-a08e-2e0bf23a75a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "902f00a3-dbc1-4ea8-bd19-3f495e6bc678",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "56e4149f-49e7-4d2f-83c1-29d4762217a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "56191401-7ff3-4fc8-a843-c93b1eb834a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "813eb70a-7e65-4b6f-bb53-3544019ab1e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "8bb0464d-3952-4f39-b845-7c4e3a528dfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "f9fa1379-4d3c-4dfd-abee-2239efe8d46c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "f8abf14c-c94a-46d9-b688-ea4190f3ca66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "4985c5e2-78a6-48dc-bcb3-14f1b7084a3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "1ba1ab2b-35d0-4735-81cd-62b7d6337894",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "34421801-5155-45ab-a591-e7b6773f7a6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "41c3bd3f-d3b1-46d4-83bb-9254497df6d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "1e739af4-aa9f-4904-83df-6328fe63960a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "0c6e3f97-fe76-4f40-b1d1-28ac0f0531f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 10,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}