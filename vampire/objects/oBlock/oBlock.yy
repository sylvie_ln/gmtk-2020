{
    "id": "67f21fae-a217-4a9e-9eb4-cd5a09b8f4a7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBlock",
    "eventList": [
        {
            "id": "a98e51ee-34ac-48f5-ad99-b81a737e7664",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "67f21fae-a217-4a9e-9eb4-cd5a09b8f4a7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "bff61b7d-c694-4d6d-8c65-77603db4d943",
    "visible": true
}