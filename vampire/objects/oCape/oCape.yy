{
    "id": "69fae3e3-8252-4ed9-8c9a-bfc24d105a94",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCape",
    "eventList": [
        {
            "id": "b934f1bc-7b98-47e8-8075-7e35c58b0f21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "69fae3e3-8252-4ed9-8c9a-bfc24d105a94"
        },
        {
            "id": "0990e0e4-c532-44e0-9a35-3924efc7c47d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "69fae3e3-8252-4ed9-8c9a-bfc24d105a94"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b25c28fc-c471-4cb3-8167-85f9f811dc18",
    "visible": true
}