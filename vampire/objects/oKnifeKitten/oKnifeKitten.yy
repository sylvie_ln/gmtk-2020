{
    "id": "57032c5a-db71-43ac-abd8-5e73e4f8e878",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oKnifeKitten",
    "eventList": [
        {
            "id": "e8c8912c-3ecf-45f9-b436-b7e03076f8fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "57032c5a-db71-43ac-abd8-5e73e4f8e878"
        },
        {
            "id": "6a9a700c-7f5e-466c-bdaf-2116feee31fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "57032c5a-db71-43ac-abd8-5e73e4f8e878"
        },
        {
            "id": "adb41e0d-332e-4ea2-840e-753636a3fa9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "57032c5a-db71-43ac-abd8-5e73e4f8e878"
        },
        {
            "id": "11617cb8-1e97-4804-899a-586426e6e3f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "57032c5a-db71-43ac-abd8-5e73e4f8e878"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "953f20b4-ff9f-4745-85ee-8d23a9ee7c0e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e056d818-f37e-4e47-a35b-194cf5cd283b",
    "visible": true
}