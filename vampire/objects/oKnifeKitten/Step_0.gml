if place_meeting(x,y,oDracula) {
	global.collected[?room] = true;
	global.kittens--;
	instance_destroy();
	exit;
}

var hdir = sign(oDracula.x-x);
if hdir != 0 {
	image_xscale = hdir; 
}

var ong = !place_free(x,y+1);
if !ong {
	vv += grav;	
}
if can_jump and jump and ong {
	vv -= sqrt(2*grav*36);
	jump = false;
}
if !move(vv,1) { vv = 0; }