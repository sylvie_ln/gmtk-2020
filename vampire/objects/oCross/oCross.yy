{
    "id": "a76275d2-17bd-4535-8e9e-c142b16d7ee3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCross",
    "eventList": [
        {
            "id": "a480b2f1-bde5-4bd1-9bc6-983959d88bff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a76275d2-17bd-4535-8e9e-c142b16d7ee3"
        },
        {
            "id": "e0e66e54-4792-4914-a32c-91b92826d456",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a76275d2-17bd-4535-8e9e-c142b16d7ee3"
        },
        {
            "id": "068563c1-1eff-4ea1-9e23-fd9f6e5dd7c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "a76275d2-17bd-4535-8e9e-c142b16d7ee3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bdd04ffb-9616-4023-adc1-faa1d3a1b8a4",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "73932b6b-c231-4063-9938-ef25a3ac2804",
    "visible": true
}