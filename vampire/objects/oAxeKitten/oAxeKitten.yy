{
    "id": "bd09a691-ab2d-45bd-867a-534dcacdef3a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oAxeKitten",
    "eventList": [
        {
            "id": "9a02ec6f-ac92-4865-8f77-f0a6312f7b94",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bd09a691-ab2d-45bd-867a-534dcacdef3a"
        },
        {
            "id": "1c9b4441-212b-4315-bf96-22fb32a664b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "bd09a691-ab2d-45bd-867a-534dcacdef3a"
        },
        {
            "id": "b66953d7-5f76-4a78-93d0-b0482ed47953",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bd09a691-ab2d-45bd-867a-534dcacdef3a"
        },
        {
            "id": "3439f5a4-e40f-4d5e-be66-59d2f22b10c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "bd09a691-ab2d-45bd-867a-534dcacdef3a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "953f20b4-ff9f-4745-85ee-8d23a9ee7c0e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e056d818-f37e-4e47-a35b-194cf5cd283b",
    "visible": true
}