var knife = instance_create_depth(x,y,depth-10,oAxe);
knife.hspeed = random_range(1,2)*image_xscale;
knife.vspeed = -sqrt(2*0.2*random_range(min_height,max_height));
alarm[0] = attack_time;