event_inherited();
attack_init = room_speed/2;
attack_time = room_speed*(3/4);
jump_init = -1;
jump_time = room_speed*2;
alarm[1] = jump_init;
alarm[0] = attack_init;
jump = false;
can_jump = true;
min_height = 36;
max_height = 72;
if ds_map_exists(global.collected,room) { instance_destroy(); }