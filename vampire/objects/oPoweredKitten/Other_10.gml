if got { exit; }
var ong =!place_free(x,y+1);
var sdist = x - oDracula.x;
var dist = abs(sdist);
var rspd = 2;
var bdash = 6;
var tspd = 6;
var bspd = 2;
#region // Roc's Wing Kitten, Castle Top
if room == rm31 {
	if trigger_activated {
		if check_tag("right") {
			hv = -rspd;	
			gx = 3;
			gy = 2;
		} else {
			hv = rspd;
			gx = 4;
			gy = 1;	
		}
	}
} else
if room == rm32 {
	if trigger_active {
		hv = rspd;
		if ong {
			jump = true;	
		}
		gx = 4;
		gy = 2;	
	}
} else
if room == rm42 {
	if trigger_active {
		gx = 4;
		gy = 1;	
		if x >= 272 { 
			x = 272;
			hv = 0;
			roc_jump = true;
		} else {
			hv = rspd;	
		}
	}
} else
if room == rm41 {
	if trigger_activated  {
		hv = -rspd;
		if check_tag("right") {
			gx = 3;
			gx = 1;
		}
	} 
	if trigger_active and check_tag("left") {
		if 0 <= sdist and sdist < 48 and ong {
			roc_jump = true;
		} else if 8 < sdist and sdist < 48 and !ong and vv > 0 and hv < 0 {
			if sprite_index != sKittenT {
				event_user(10);
			}
		} else {
			if sdist > 80 and sprite_index != sKittenT and hv > 0 {
				event_user(10);
			}
			if x > 200 {
				x = 200;
				event_user(10);
			}
		}
	}
} else
#endregion
#region // Double Jump Kitten, Bottom Left
if room == rm26 {
	if trigger_activated {
		if check_tag("right") {
			hv = -rspd;	
		} else {
			hv = rspd;
			gx = 3;
			gy = 6;
			jump = true;
		}
	} else {
		jump = true;
		if !ong and vv > 1 {
			djump = true;
		}
	}
	if trigger_active {
		if check_tag("right") {
			if (y < oDracula.bbox_top-8 and vv < 0) or (y > oDracula.bbox_bottom+24 and dist < 48) or sdist > 16 {
				if hv < 0 {
					event_user(10);	
				} else {
					hv = rspd;
				}
				if !place_free(x+32,y) and !place_meeting(x,y+24,oDracula) {
					if ong {
						jump = true;
					} else if !ong and vv > 0 {
						djump = true;
					}
				}
			} else if dist < 64 {
				var phv = hv;
				var nhv = sign(sdist)*rspd;
				if sign(nhv) == -sign(phv) {
					hv = -nhv;
					event_user(10);
				} else {
					hv = nhv;	
				}
				if sdist <= 16 and collision_rectangle(x-32,y+24,x+32,y-48,oDracula,false,true) != noone {
					if ong {
						jump = true;
					} else if !ong and vv > -1 {
						djump = true;
					}
				}
			}
			if x+hv < 72 {
				x = 72;
				if hv < 0 {
					event_user(10);	
				} else {
					hv = rspd;
				}
			}
			if y > oDracula.bbox_bottom+8 and dist > 48 and sdist <= 16 {
				jump = false;
				djump = false;
			}
		} else {
			gx = 3;
			gy = 6;
			hv = rspd;
			jump = true;
		}
	}
} else
if room == rm36 {
	if trigger_activated {
		if check_tag("right") {
			hv = -rspd;	
			gx = 2;
			gy = 6;
		} else {	
			hv = rspd;
			gx = 4;
			gy = 6;
		}
	}
	if trigger_active {
		if check_tag("left") {
			if vv > 1 {
				djump = true;	
			}
		} else {
			if vv > 2 {
				djump = true;	
			}
		}
	}
} else
if room == rm46 {
	if trigger_activated {
		hv = -rspd;
		jump = true;
		gx = 3;
		gy = 6;
	}
	if trigger_active {
		if collision_rectangle(x-32,y+24,x+32,y-48,oDracula,false,true) != noone {
			djump = true;	
		}
	}
} else
#endregion
#region // Wall Jump Kitten, Bottom Right
if room == rm57 {
	if trigger_activated {
		hv = rspd;
		jump = true;
		gx = 5;
		gy = 6;
	}
	if trigger_active {
		can_wjump = true;
	}
}
if room == rm56 {
	if trigger_activated {
		hv = -rspd;
	}
	if trigger_active {
		can_wjump = true;
		if ong and (x div (global.rw/2)) == (oDracula.x div (global.rw/2)) and (!place_free(x+24*sign(hv),y) or place_free(x+16*sign(hv),y+16)) {
			jump = true;
			can_wjump = false;
		}
		if ong and (x div (global.rw/2)) != (oDracula.x div (global.rw/2)) {
			if min(global.rw-x,x) > 56 {
				hv = rspd*sign(x-(global.rw/2));
			} else {
				hv = 0;	
			}
		} else if ong and dist < 32 and hv == 0 and y == 88 {
			hv = sign(sdist)*rspd;
		}
		/*
		if sign(hv) == -sign(sdist) and collision_rectangle(x,y-64,x+sign(hv)*48,bbox_bottom+24,oDracula,false,true) != noone {
			event_user(10);	
		}
		*/
	}
}
#endregion
#region // Bat Forme Kitten, Top Left
if room == rm01 {
	if trigger_active {
		if check_tag("bottom") {
			bat = true;
			if y+vv > 56 and x+hv < 200 {
				vv = -bspd*2;	
			} else if y+vv < 56 and x+hv < 192 {
				y = 56;
				vv = 0;
				hv = bspd;
			} else if x+hv > 200 and y+vv > 40 {
				x = 200;
				hv = 0;
				vv = -bspd;
			} else if y+vv < 40 {
				vv = 0;
				y = 40;
				hv = bspd;
			}
			if !place_free(x+hv,y) {
				hv = -hv;	
			}
		} else {
			if x == xstart {
				hv = rspd;	
			}
			if !bat and place_free(x,y+1) {
				bat = true;
				hv = 0;
				vv = -bspd*2;
			}
			if y+vv < 40 {
				vv = 0;
				y = 40;
				hv = bspd;
			}
			if !place_free(x+hv,y) {
				hv = -hv;	
			}
		}
	}
}
#endregion
#region // Backdash Kitten, Center
if room == rm55 {
	if trigger_active {
		if check_tag("right") {
			if ong and backdash == 0 {
				hv = 0;
				backdash = -bdash;	
			}
			gx = 4;
			gy = 5;
		} else {
			if ong and backdash == 0 {
				hv = 0;
				backdash = bdash;	
			}
			gx = 6;
			gy = 5;
		}
	}
} else
if room == rm45 {
	image_xscale = 1;
	if trigger_active {
		if ong and backdash == 0 {
			backdash = -bdash;	
		}
		hv = 0;
		gx = 3;
		gy = 5;
	}
} else
if room == rm35 {
	if trigger_active {
		if oDracula.y < 100 {
			gx = 5;
			gy = 5;
			hv = rspd;
			helpme = false;
		}
		if gx != -1 and gy != -1 { 
			exit;
		}
		if !place_free(x-1,y) {
			if (hv == 0 and sprite_index != sKittenT) or hv > 0 {
				hv = rspd;	
				event_user(10);
				helpme = true;
			}
		} else if ong and backdash == 0 {
			hv = 0;
			backdash = -bdash;	
		}
	}
} else 
if room == rm65 {
	image_xscale = -1;
	if trigger_active {
		if ong and backdash == 0 {
			backdash = bdash;	
		}
		hv = 0;
		gx = 6;
		gy = 6;
	}
} else
if room == rm66 {
	image_xscale = -1;
	if trigger_active {
		if ong and backdash == 0 {
			backdash = bdash;	
		}
		hv = 0;
		gx = 7;
		gy = 6;
	}
} else
if room == rm76 {
	if trigger_active {
		if oDracula.x >= 200 or oDracula.y <= 32 {
			gx = 5;
			gy = 5;
			hv = -rspd;
			helpme = false;
		}
		if gx != -1 and gy != -1 { 
			exit;
		}
		if !place_free(x+1,y) {
			if (hv == 0 and sprite_index != sKittenT) or hv < 0 {
				hv = -rspd;	
				event_user(10);
				helpme = true;
			}
		} else if ong and backdash == 0 {
			hv = 0;
			backdash = bdash;	
		}
	}
} else 
#endregion
#region // Tackle Kitten, Bottom Center
if room == rm37 {
	if dist < 64 and ia == 0 and hv == 0 {
		tackle = tspd;	
	}
	if ia != 0 and tackle == 0 {
		ia = 0;
		hv = -1.5;
	}
	if hv != 0 and x < xstart {
		x = xstart;
		image_xscale = -1;
		hv = 0;
	}
}
#endregion
{ }