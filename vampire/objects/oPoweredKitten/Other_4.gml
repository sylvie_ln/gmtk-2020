if got { exit; }
active = global.rx == cx and global.ry == cy;
visible = active;
trigger_activated = false;
trigger_active = false;
ds_map_clear(trigger_tags);
trail = false;
ds_list_clear(trail_list);
hv = 0;
vv = 0;
prev_hv = hv;
jump = false;
djump = false;
roc_jump = false;
can_wjump = false;
wjump = false;
wjmp_launch = 0;
backdash = 0;
tackle = 0;
ia = 0;
bat = false;
sprite_index = sKittenS;
if !helpme {
	var location = instance_nearest(x,y,oKittenLocation);
	if location != noone {
		x = location.x;
		y = location.y;
	}
}