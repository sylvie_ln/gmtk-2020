if got { exit; }
#region // Roc's Wing Kitten Reset
if cx == 4 and cy == 2 
or cx == 3 and cy == 2 {
	if room == rm41 {
		cx = 3;
		cy = 1;
		image_xscale = 1;
	}
}
#endregion
#region // Double Jump Kitten Reset
if cx = 3 and cy == 6
or cx = 4 and cy == 6{
	if room == rm15 
	or room == rm45 {
		cx = 2;
		cy = 6;
		image_xscale = 1;
	}
}
#endregion

#region // Backdash Kitten Reset
if cx == 3 and cy == 5 {
	if room == rm34 or room == rm65 {
		cx = 5;
		cy = 5;
		image_xscale = 1;
		helpme = false;
	}
}
if cx == 7 and cy == 6
or cx == 6 and cy == 6
or cx == 6 and cy == 5 {
	if room == rm64 or room == rm75 {
		cx = 5;
		cy = 5;
		image_xscale = 1;
		helpme = false;
	}
}
#endregion