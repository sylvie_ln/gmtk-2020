if got { exit; }

event_user(1);

if !active { exit; }

if active and place_meeting(x,y,oDracula) and tackle == 0  {
	got = true;
	global.kittens--;
	if pow != "" {
		variable_instance_set(oDracula.id,pow,true);
		switch(pow) {
			case "can_djump":
				show_message("Gained DoubleJump.\nPress X/Space in Air to DoubleJump");
			break;
			case "can_roc":
				show_message("Gained FlyHigh.\nPress Up + X/Space in Air to FlyHigh");
			break;
			case "can_backdash":
				show_message("Gained BackDash.\nPress Down + C/Control on Ground to BackDash");
			break;
			case "can_wjump":
				show_message("Gained WallJump.\nPress X/Space while Facing Wall to WallJump");
			break;
			case "can_tackle":
				show_message("Gained TackleRush.\nPress C/Control on Ground to TackleRush");
			break;
			case "can_bat":
				show_message("Gained BecomeBat.\nPress Up + C/Control to BecomeBat");
			break;
		}
	}
	exit;
}

if !trigger_active {
	with oDracula {
		var trigger = instance_place(x,y,oKittenTrigger);
		if trigger != noone  {
			other.trigger_activated = true;
			other.trigger_active = true;
			ds_map_copy(other.trigger_tags,trigger.tags);
		}
	}
}

event_user(0);

trigger_activated = false;

if wjump and !place_free(x+image_xscale,y) and wjumped != image_xscale {
	vv = -wjmp;
	wjump = false;
	wjumped = image_xscale;
	wjmp_launch = -image_xscale*wjmp_power;
	trail = true;
}

if hv != 0 {
	image_xscale = sign(hv);
	image_speed = abs(hv);
	sprite_index = sKittenR;
} else if sprite_index != sKittenT {
	image_speed = 0.5;
	sprite_index = sKittenS;	
} else {
	image_speed = 1;	
}

if trail {
	ds_list_add(trail_list,[x,y]);	
	if ds_list_size(trail_list) > trail_length {
		ds_list_delete(trail_list,0);
	} 
}

if wjmp_launch != 0 {
	hv = wjmp_launch;
	if abs(wjmp_launch) > 2 {
		wjmp_launch *= 0.9;	
	} else {
		wjmp_launch = 0;	
	}
}


if tackle != 0 {
	hv = tackle;
	trail = true;
	image_xscale = sign(hv);
	if abs(tackle) > 1 {
		tackle *= 0.95;	
	} else {
		tackle = 0;	
		trail = false;
		ds_list_clear(trail_list);
	}
}


if backdash != 0 {
	hv = backdash;
	trail = true;
	sprite_index = sKittenS;
	image_xscale = -sign(hv);
	if abs(backdash) > 1 {
		backdash *= 0.9;	
	} else {
		backdash = 0;	
		trail = false;
		ds_list_clear(trail_list);
	}
}

if !move(hv,0) {
	event_user(10);
	wjmp_launch = 0;
	backdash = 0;
}

var ong = !place_free(x,y+1);
if !ong {
	if !(sprite_index == sKittenT and can_wjump)
	and tackle == 0 
	and !bat {
		vv += grav;	
	}
} else {	
	can_djump = true;
	wjumped = 0;
}
	

var can_jump = ong;
if jump and can_jump {
	vv = -jmp;
	jump = false;
	djump = false;
}
if djump and can_djump {
	vv = -djmp;
	jump = false;
	djump = false;
	can_djump = false;
	if !ong {
		trail = true;
	}
}
if roc_jump {
	vv = -roc;
	trail = true;
	roc_jump = false;
}

if !move(vv,1) {
	vv = 0;	
}

if vv >= 0 and trail and backdash == 0 and tackle == 0 {
	trail = false;	
	ds_list_clear(trail_list);
}

if bat {
	sprite_index = sKittenBat;
	image_speed = 1;
}

var pcx = cx;
var pcy = cy;
if x < 0 {
	cx -= 1;
	x += global.rw;
} if x >= global.rw {
	cx += 1;
	x -= global.rw;
}
if y < 0 {
	cy -= 1;
	y += global.rh;
}
if y >= global.rh {
	cy += 1;
	y -= global.rh;
}
if pcx != cx or pcy != cy {
	image_xscale = sign(oDracula.x-x);
	event_perform(ev_other,ev_room_start);
}