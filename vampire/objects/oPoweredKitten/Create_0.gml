event_inherited();
cx = global.rx;
cy = global.ry;
active = false;
trigger_activated = false; 
trigger_active = false; 
trigger_tags = ds_map_create();
prev_hv = hv;
jump = false;
jmp = sqrt(2*grav*36);
djump = false;
can_djump = true;
djmp = jmp;
can_wjump = false;
wjump = false;
wjmp = sqrt(2*grav*24);
wjumped = false;
wjmp_launch = 0;
wjmp_power = 4;
roc_jump = false;
roc = sqrt(2*grav*240);
backdash = 0;
tackle = 0;
ia = 0;
trail = false;
trail_length = 8;
trail_list = ds_list_create();
gx = -1;
gy = -1;
bat = false;
helpme = false;
got = false;
pow = "";