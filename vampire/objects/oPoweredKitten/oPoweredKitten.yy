{
    "id": "d5bc83a9-ae81-48bf-b51d-b9a0f12787bb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPoweredKitten",
    "eventList": [
        {
            "id": "496def60-4162-4bc9-8744-609fb06fab02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d5bc83a9-ae81-48bf-b51d-b9a0f12787bb"
        },
        {
            "id": "2f2aef08-06a5-4f98-a47f-f9c384f6254c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "d5bc83a9-ae81-48bf-b51d-b9a0f12787bb"
        },
        {
            "id": "756dacd3-9c12-4c2c-8a97-3b099b7868c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d5bc83a9-ae81-48bf-b51d-b9a0f12787bb"
        },
        {
            "id": "1bea4a3a-0246-49c9-b51d-39b1c12698c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "d5bc83a9-ae81-48bf-b51d-b9a0f12787bb"
        },
        {
            "id": "925e69ae-bc85-461c-9ce5-6141ef19992d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "d5bc83a9-ae81-48bf-b51d-b9a0f12787bb"
        },
        {
            "id": "a74b57a9-85a8-410b-bfae-0d9080bc167e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "d5bc83a9-ae81-48bf-b51d-b9a0f12787bb"
        },
        {
            "id": "22654d01-a1ca-4a19-a129-c23f3fcd9a4c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d5bc83a9-ae81-48bf-b51d-b9a0f12787bb"
        },
        {
            "id": "0aa1c0dc-449a-45f6-877c-452890efaddc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 20,
            "eventtype": 7,
            "m_owner": "d5bc83a9-ae81-48bf-b51d-b9a0f12787bb"
        },
        {
            "id": "fe90931e-a443-4b5e-966a-35adb4039300",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "d5bc83a9-ae81-48bf-b51d-b9a0f12787bb"
        },
        {
            "id": "14ec12db-e7ad-4a6f-a404-bf0f81ed236b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f99aebbe-de21-402b-88cf-8192ba76b130",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d5bc83a9-ae81-48bf-b51d-b9a0f12787bb"
        }
    ],
    "maskSpriteId": "db29d9f6-82f6-4c04-9114-6cdd19ce45ae",
    "overriddenProperties": null,
    "parentObjectId": "953f20b4-ff9f-4745-85ee-8d23a9ee7c0e",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e056d818-f37e-4e47-a35b-194cf5cd283b",
    "visible": true
}