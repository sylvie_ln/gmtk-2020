{
    "id": "b330759b-77e5-410c-9b69-bb61bf6ad2c9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDeadProjectile",
    "eventList": [
        {
            "id": "aa9269fb-91fa-4aa4-b415-c6f842531b8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "b330759b-77e5-410c-9b69-bb61bf6ad2c9"
        },
        {
            "id": "07bee52d-18ac-4985-9bdd-5d8049635102",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b330759b-77e5-410c-9b69-bb61bf6ad2c9"
        },
        {
            "id": "7fafaf27-4b33-48e3-a7e4-c5687fbc427c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b330759b-77e5-410c-9b69-bb61bf6ad2c9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}