{
    "id": "f0008885-329c-4075-9e17-841224c1ce35",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oKitten",
    "eventList": [
        {
            "id": "1d21d0a5-1543-459c-b23d-e1e798866aa0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f0008885-329c-4075-9e17-841224c1ce35"
        },
        {
            "id": "60c2a233-7d18-4176-ab89-576dddaaacc0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f0008885-329c-4075-9e17-841224c1ce35"
        },
        {
            "id": "c886330b-912a-4637-ba29-aabc875ab316",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f0008885-329c-4075-9e17-841224c1ce35"
        },
        {
            "id": "423ba41f-19aa-48ef-a5ec-5588cffc24d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "f0008885-329c-4075-9e17-841224c1ce35"
        },
        {
            "id": "78bf384e-9c80-438c-a739-ba56c09a274e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "f0008885-329c-4075-9e17-841224c1ce35"
        },
        {
            "id": "8177b43a-643e-4b50-90f9-701856550859",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "f0008885-329c-4075-9e17-841224c1ce35"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "953f20b4-ff9f-4745-85ee-8d23a9ee7c0e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "83dea948-e4e5-460d-a5be-19061f960c73",
    "visible": true
}