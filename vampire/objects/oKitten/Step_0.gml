if place_meeting(x,y,oDracula) {
	global.collected[?room] = true;
	global.kittens--;
	instance_destroy();
	exit;
}
var ong = !place_free(x,y+1);

var seen_this_frame = 
(hv != 0 and y-oDracula.y <= 24 and y-oDracula.y >= -24 
 and collision_line(x,oDracula.y,x+image_xscale*64,oDracula.y,oDracula,false,true) != noone);
var at_edge = (hv != 0 and !place_free(x,y+28) and place_free(x+image_xscale*16,y+28));
if at_edge or (seen_this_frame and !seen) or !move(hv,0) {
	if ong {
		hv = 0;
	}
	if seen_this_frame or !seen {
		seen = seen_this_frame;
		alarm[1] = room_speed
	}
	alarm[0] = 1;
	sprite_index = sKittenS;
}
if sprite_index == sKittenR {
	image_speed = spd;
} else {
	image_speed = 1;
}
if ong and place_meeting(x+sign(image_xscale)*24,y,oDracula) {
	jump = true;	
}

if !ong {
	vv += grav;	
}
if jump and ong {
	vv -= sqrt(2*grav*36);
	jump = false;
}
if !move(vv,1) { vv = 0; }