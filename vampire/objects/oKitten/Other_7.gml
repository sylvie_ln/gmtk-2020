if sprite_index == sKittenT {
	image_xscale = -image_xscale;
	if seen {
		spd = 2;	
	} else {
		spd = 0.5;	
	}
	hv = image_xscale*spd;	
	
	alarm[0] = room_speed*3;
	sprite_index = sKittenR;
}