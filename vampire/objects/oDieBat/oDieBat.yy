{
    "id": "0d140b53-374f-4153-87bd-1cd73170513c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDieBat",
    "eventList": [
        {
            "id": "a9812625-6342-435e-8bba-09255c3ebdb5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "0d140b53-374f-4153-87bd-1cd73170513c"
        },
        {
            "id": "3a4d2ced-7c07-4970-8030-73d5fe2cda65",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0d140b53-374f-4153-87bd-1cd73170513c"
        },
        {
            "id": "ec6ea306-7eac-41e9-82cd-083eb2a1a10f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0d140b53-374f-4153-87bd-1cd73170513c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2dd45b51-567c-4b8f-9a75-c3376f069307",
    "visible": true
}