{
    "id": "85a9507f-c6c8-456a-9652-1e9a2d09cc07",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oKittenTrigger",
    "eventList": [
        {
            "id": "68049c86-d575-4e30-9248-27876bbe2123",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "85a9507f-c6c8-456a-9652-1e9a2d09cc07"
        },
        {
            "id": "ac44c382-2e3c-4783-8c4c-4b0ecc1fed6b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "85a9507f-c6c8-456a-9652-1e9a2d09cc07"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fb70aec5-b4eb-429d-8cc6-9deac74e1069",
    "visible": false
}