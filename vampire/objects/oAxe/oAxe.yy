{
    "id": "2a16c9d5-95f8-4c84-999c-a3a096f5cf88",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oAxe",
    "eventList": [
        {
            "id": "3b6f9763-b39a-444b-aa50-1bec4d1ffe8d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2a16c9d5-95f8-4c84-999c-a3a096f5cf88"
        },
        {
            "id": "346bbace-c2a7-4c2f-8d89-1bd6eaec1afb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2a16c9d5-95f8-4c84-999c-a3a096f5cf88"
        },
        {
            "id": "6ec431b1-8215-478d-bed5-64dd456f1e6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "2a16c9d5-95f8-4c84-999c-a3a096f5cf88"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bdd04ffb-9616-4023-adc1-faa1d3a1b8a4",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ca51dfdc-df83-4dda-8e26-1180714ac904",
    "visible": true
}