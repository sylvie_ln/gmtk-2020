global.view_w = room_width;
global.view_h = room_height;
global.scale = floor(min(display_get_width()/global.view_w,display_get_height()/global.view_h));
window_set_size(global.view_w*global.scale,global.view_h*global.scale);
instance_create_depth(0,0,0,oInput);
instance_create_depth(0,0,0,oHUD);
global.kittens = 25;
global.collected = ds_map_create();
alarm[0] = 1;
	