{
    "id": "24542abe-203d-4b19-be33-0bd126dc804a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHUD",
    "eventList": [
        {
            "id": "d24f601d-cc86-447e-aa2a-9bc05583f85f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "24542abe-203d-4b19-be33-0bd126dc804a"
        },
        {
            "id": "8231a749-7bf0-43bc-b070-16e7afa8e98f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "24542abe-203d-4b19-be33-0bd126dc804a"
        },
        {
            "id": "3dac8f34-7580-4a35-b77b-39d1ca3db08f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "24542abe-203d-4b19-be33-0bd126dc804a"
        },
        {
            "id": "d68e7c05-1fc9-44ac-a14b-82da713dea1b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "24542abe-203d-4b19-be33-0bd126dc804a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "83dea948-e4e5-460d-a5be-19061f960c73",
    "visible": true
}