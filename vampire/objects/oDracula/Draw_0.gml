if trail {
	//gpu_set_blendmode(bm_add);
	for(var i=ds_list_size(trail_list)-1; i>=0; i--) {
		var pos = trail_list[|i];
		if tackle != 0 {	
			draw_sprite_ext(sTackleDracle,image_index,pos[0],pos[1],image_xscale,image_yscale,image_angle,merge_color(c_red,c_fuchsia,0.5),1-((ds_list_size(trail_list)-i)/ds_list_size(trail_list)));
		}
		draw_sprite_ext(sprite_index,image_index,pos[0],pos[1],image_xscale,image_yscale,image_angle,merge_color(c_red,c_fuchsia,0.5),1-((ds_list_size(trail_list)-i)/ds_list_size(trail_list)));
	}
	//gpu_set_blendmode(bm_normal);
}
if tackle != 0 {	
	draw_sprite_ext(sTackleDracle,image_index,x,y,image_xscale,image_yscale,image_angle,c_white,1);
}
draw_self();