if drac {
	sprite_index = sDracula;
	if input_pressed(oInput.act1) or input_pressed(oInput.act2) {
		drac = false;
		sprite_index = sDracPose;
		instance_create_depth(x,y,depth+1,oCape);
	}
	exit;
}

if sprite_index == sDracPose {
	if !instance_exists(oCape) {
		sprite_index = sDracS;	
	}
	exit;
}

if dead { 
	visible = false;
	if instance_exists(dracpoint) {
		x = dracpoint.x;
		y = dracpoint.y;
	}
	if !instance_exists(oDieBat) {
		dead = false;
		visible = true;
		sprite_index = sDracS;
	}
	exit; 
}

var kittendie = false;
with instance_place(x,y,oPoweredKitten) {
	if tackle != 0 { kittendie = true; }	
}
if place_meeting(x,y,oDeath) or kittendie {
	hv = 0;
	vv = 0;
	bat = false;
	tackle = 0;
	backdash = false;
	djump_count = 1;
	ds_list_clear(trail_list);
	sprite_index = sDracS;
	dead = true;
	repeat(32) {
		instance_create_depth(x,y,depth-1,oDieBat);	
	}
	exit;
}

if trail {
	ds_list_add(trail_list,[x,y]);	
	if ds_list_size(trail_list) > trail_length {
		ds_list_delete(trail_list,0);
	} 
}

var ong = !place_free(x,y+1);

var hdir = input_held("Right")-input_held("Left");

if !bat {
	hv = hdir*spd;
}

if input_pressed("Act") {
	if can_bat and (bat or input_held("Up")) and backdash == 0 and tackle == 0  {
		bat = !bat;
		hv = 0;
		vv = -bspd; 
		wjmp_launch = 0;
		backdash = 0;
		tackle = 0;
	} else if !bat {
		if can_backdash and ong and input_held("Down") and backdash == 0 and tackle == 0 {
			backdash = -image_xscale*bdash;	
		} else if can_tackle and ong and backdash == 0 and tackle == 0 {
			tackle = image_xscale*tspd;
		}
	}
	if !can_bat and !can_tackle and !can_backdash {
		show_message("C doesn't do anything until you get a powerups.");
	}
}

if !bat {
	hv += wjmp_launch;
	if abs(wjmp_launch) > 1 {
		wjmp_launch *= 0.9;	
	} else {
		wjmp_launch = 0;	
	}

	if backdash != 0 {
		hv = backdash;
		trail = true;
		image_xscale = -sign(hv);
		if abs(backdash) > 1 {
			backdash *= 0.9;	
		} else {
			backdash = 0;	
			trail = false;
			ds_list_clear(trail_list);
		}
	}

	if tackle != 0 {
		hv = tackle;
		trail = true;
		image_xscale = sign(hv);
		if abs(tackle) > 1 {
			tackle *= 0.95;	
		} else {
			tackle = 0;	
			trail = false;
			ds_list_clear(trail_list);
		}
		with instance_place(x+hv,y,oBlockCrack) {
			instance_destroy();	
		}
	}
}


if bat {
	hdir = input_pressed("Right")-input_pressed("Left");
	var vdir = input_pressed("Down")-input_pressed("Up");
	if hdir != 0 {
		vv = 0;
		hv = hdir*bspd;
	}
	if vdir != 0 {
		hv = 0;	
		vv = vdir*bspd;
	}
}

if !move(hv,0) {
	if bat { hv = -hv } else {
		hv = 0;	
	}
	wjmp_launch = 0;
	backdash = 0;
	tackle /= 1.5;
}

if !bat {
	if !ong {
		if tackle == 0 {
			vv += grav;	
		}
	} else {
		djump_count = 1;	
		wjmp_launch = 0;
	}

	var can_jump = (tackle == 0 and backdash == 0) and (ong or (can_djump and djump_count > 0) or (can_roc and input_held("Up")) or (can_wjump and !place_free(x+image_xscale,y)) )
	if can_jump and input_pressed("Jump") {
		if can_roc and input_held("Up") {
			vv = -roc;
			trail = true;
		} else if ong {
			vv = -jmp;	
		} else if can_wjump and !place_free(x+image_xscale,y) {
			vv = -wjmp;
			image_xscale = -image_xscale;
			wjmp_launch = image_xscale*wjmp_power;
			trail = true;
		} else if can_djump and djump_count > 0 {
			vv = -djmp;
			djump_count--;
			trail = true;
		}
	}
	if vv < 0 and input_released("Jump") {
		vv /= 2;	
	}
}

if !move(vv,1) {
	if bat { vv = -vv } else {
		vv = 0;	
	}
}

if bat and hv != 0 {
	image_xscale = sign(hv);	
}
if hdir != 0 and tackle == 0 and backdash == 0 {
	image_xscale = hdir;	
}

if (input_released("Left") or input_released("Right")) and wjmp_launch != 0 {
	wjmp_launch += image_xscale;
}	


var spr = "sDrac";
if ong and tackle == 0 {
	if hdir != 0 or backdash != 0 {
		spr += "R";
	} else {
		spr += "S";	
		image_index = 0;
	}
} else {
	if vv <= 0 or tackle != 0 {
		spr += "J";	
	} else {
		spr += "F";
	}
}
spr = asset_get_index(spr)
if bat { spr = sDracBat; }
if sprite_exists(spr) {
	sprite_index = spr;	
}

if vv >= 0 and trail and backdash == 0 and tackle == 0 {
	trail = false;	
	ds_list_clear(trail_list);
}

var rm = -2;
if x < 0 {
	rm = room_shift(-1,0);
	if rm != -1 {
		x += global.rw;
	} else {
		x = xprevious;
	}
} if x >= global.rw {
	rm = room_shift(1,0);
	if rm != -1 {
		x -= global.rw;
	} else {
		x = xprevious;
	}
}
if y < 0 {
	rm = room_shift(0,-1);
	if rm != -1 {
		y += global.rh;
	} else {
		y = yprevious;
	}
}
if y >= global.rh {
	rm = room_shift(0,1);
	if rm != -1 {
		y -= global.rh;
	}  else {
		y = yprevious;
	}
}
if rm != -1 and rm != -2 {
	room_goto(rm);	
} else if rm == -1 {
	// trying to access nonexistent room
}