{
    "id": "f99aebbe-de21-402b-88cf-8192ba76b130",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDracula",
    "eventList": [
        {
            "id": "a4025012-5dc8-4c2b-9922-ba9991095d7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f99aebbe-de21-402b-88cf-8192ba76b130"
        },
        {
            "id": "5a7ef52a-9783-4734-906f-de2311cae832",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f99aebbe-de21-402b-88cf-8192ba76b130"
        },
        {
            "id": "fb6e09c8-294b-48fe-8899-7092f6441fce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f99aebbe-de21-402b-88cf-8192ba76b130"
        },
        {
            "id": "5d9d5771-cd24-4a62-89c4-e26f181e10fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "f99aebbe-de21-402b-88cf-8192ba76b130"
        }
    ],
    "maskSpriteId": "3d01ae64-730e-478c-bd13-19e93c5d798a",
    "overriddenProperties": null,
    "parentObjectId": "953f20b4-ff9f-4745-85ee-8d23a9ee7c0e",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "62974db5-0b5b-43bd-bcef-ed8a3e85583c",
    "visible": true
}