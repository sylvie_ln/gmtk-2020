event_inherited();
spd = 1.6;
jmp = sqrt(2*grav*36);

can_djump = false;
can_wjump = false;
can_backdash = false;
can_tackle = false;
can_roc = false;
can_bat = false;

djump_count = 1;
djmp = jmp;
wjmp = sqrt(2*grav*24);
wjmp_launch = 0;
wjmp_power = 8;
roc = sqrt(2*grav*240);
backdash = 0;
bdash = 6;
tackle = 0;
tspd = 6;
bat = false;
bspd = 2;

drac = true;

trail = false;
trail_length = 8;
trail_list = ds_list_create();

dracpoint = noone;
dead = false;