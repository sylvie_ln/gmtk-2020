{
    "id": "b5a4fd15-ff5f-435b-8860-98b06bce469e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCrossKitten",
    "eventList": [
        {
            "id": "7958fe3d-b2ab-4b93-b08f-4d3597091144",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b5a4fd15-ff5f-435b-8860-98b06bce469e"
        },
        {
            "id": "ecc318c3-d296-4e0c-af8e-bafce74b4585",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "b5a4fd15-ff5f-435b-8860-98b06bce469e"
        },
        {
            "id": "454a63d3-4707-49f2-84ef-fb29fb395255",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b5a4fd15-ff5f-435b-8860-98b06bce469e"
        },
        {
            "id": "7306a000-4a15-4ba2-905a-0e14a24c1498",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b5a4fd15-ff5f-435b-8860-98b06bce469e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "953f20b4-ff9f-4745-85ee-8d23a9ee7c0e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e056d818-f37e-4e47-a35b-194cf5cd283b",
    "visible": true
}