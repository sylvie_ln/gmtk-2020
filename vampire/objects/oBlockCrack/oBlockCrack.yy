{
    "id": "b272bd2d-eeea-42c1-9041-ed23dfa5cf01",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBlockCrack",
    "eventList": [
        {
            "id": "f03cdba5-6140-42ca-af03-d1995a91337e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b272bd2d-eeea-42c1-9041-ed23dfa5cf01"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "1e44e096-bdd9-46c9-a138-a05398a13acd",
    "visible": true
}