{
    "id": "4c6d44bb-1a62-48c9-9c43-14d749fc8cb6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oKnife",
    "eventList": [
        {
            "id": "bcadc682-7cbd-4dcb-a34f-bd013fc89cd4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4c6d44bb-1a62-48c9-9c43-14d749fc8cb6"
        },
        {
            "id": "39cec92f-2cb8-43f8-99e0-f006d38d52a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4c6d44bb-1a62-48c9-9c43-14d749fc8cb6"
        },
        {
            "id": "2895decb-9819-44cf-938b-fae130c1d210",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "4c6d44bb-1a62-48c9-9c43-14d749fc8cb6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bdd04ffb-9616-4023-adc1-faa1d3a1b8a4",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b1e26ff6-5cc6-4547-9c16-e4671f847d9b",
    "visible": true
}