if !place_free(x,y) {
	hspeed = 0;
	vspeed = -1;
	instance_change(oDeadProjectile,false);
	exit;
} else {
	image_xscale = sign(hspeed);
}